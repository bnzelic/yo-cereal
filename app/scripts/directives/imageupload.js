angular.module('imageupload', [])
    .directive('image', function($q, ParseService) {
        'use strict'

        var URL = window.URL || window.webkitURL;

/*        var getResizeArea = function() {
            var resizeAreaId = 'fileupload-resize-area';

            var resizeArea = document.getElementById(resizeAreaId);

            if (!resizeArea) {
                resizeArea = document.createElement('canvas');
                resizeArea.id = resizeAreaId;
                resizeArea.style.visibility = 'hidden';
                document.body.appendChild(resizeArea);
            }

            return resizeArea;
        }

        var resizeImage = function(origImage, options) {
            var maxHeight = options.resizeMaxHeight || 3000;
            var maxWidth = options.resizeMaxWidth || 3000;
            var quality = options.resizeQuality || 1.0;
            var type = options.resizeType || 'image/jpeg';

            var canvas = getResizeArea();

            var height = origImage.height;
            var width = origImage.width;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > maxWidth) {
                    height = Math.round(height *= maxWidth / width);
                    width = maxWidth;
                }
            } else {
                if (height > maxHeight) {
                    width = Math.round(width *= maxHeight / height);
                    height = maxHeight;
                }
            }

            canvas.width = width;
            canvas.height = height;

            //draw image on canvas
            var ctx = canvas.getContext("2d");
            ctx.drawImage(origImage, 0, 0, width, height);

            // get the data from canvas as 70% jpg (or specified type).
            return canvas.toDataURL(type, quality);
        };


        function cropImage(url, options, callback) {
            var maxHeight = options.resizeMaxHeight || 3000;
            var maxWidth = options.resizeMaxWidth || 3000;
            var quality = options.resizeQuality || 1.0;
            var type = options.resizeType || 'image/jpeg';

            var canvas = getResizeArea();

            canvas.width = maxWidth;
            canvas.height = maxHeight;

            var context = canvas.getContext("2d");
            var imageObj = new Image();
            var pixelRatio = window.devicePixelRatio;
            context.scale(pixelRatio, pixelRatio);

            imageObj.onload = function() {
                var sourceX = 0;
                var sourceY = 0;
                var destX = 0;
                var destY = 0;

                if (canvas.width > canvas.height) {
                    var stretchRatio = (imageObj.width / canvas.width);
                    var sourceWidth = Math.floor(imageObj.width);
                    var sourceHeight = Math.floor(canvas.height * stretchRatio);
                    sourceY = Math.floor((imageObj.height - sourceHeight) / 2);
                } else {
                    var stretchRatio = (imageObj.height / canvas.height);
                    var sourceWidth = Math.floor(canvas.width * stretchRatio);
                    var sourceHeight = Math.floor(imageObj.height);
                    sourceX = Math.floor((imageObj.width - sourceWidth) / 2);
                }
                var destWidth = Math.floor(canvas.width / pixelRatio);
                var destHeight = Math.floor(canvas.height / pixelRatio);

                context.drawImage(imageObj, sourceX, sourceY, sourceWidth,
                    sourceHeight, destX, destY, destWidth, destHeight);

                callback(canvas.toDataURL(type, quality));

            };
            imageObj.src = url;
        }



        var createImage = function(url, callback) {
            var image = new Image();
            image.onload = function() {
                callback(image);
            };
            image.src = url;
        };*/

        var fileToDataURL = function(file) {
            var deferred = $q.defer();
            var reader = new FileReader();
            reader.onload = function(e) {
                deferred.resolve(e.target.result);
            };
            reader.readAsDataURL(file);
            return deferred.promise;
        };

/*        function dataURItoBlob(dataURI) {
            var binary = atob(dataURI.split(',')[1]);
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: 'image/jpeg'
            });
        };
*/

        return {
            restrict: 'A',
            scope: {
                image: '=',
                resizeMaxHeight: '@',
                resizeMaxWidth: '@',
                resizeQuality: '@',
                resizeType: '@',
            },
            link: function postLink(scope, element, attrs, ctrl) {

                /*var doResizing = function(imageResult, callback) {
                    cropImage(imageResult.url, scope, function(dataURL) {
                        imageResult.resized = {
                            dataURL: dataURL,
                            type: dataURL.match(/:(.+\/.+);/)[1],
                        };
                        callback(imageResult);
                    });
                    console.log("resized");
                };*/

                var applyScope = function(imageResult) {
                    scope.$apply(function() {
                        scope.image = imageResult
                        console.log("Image in DOM");
                    });
                };


                element.bind('change', function(evt) {
                    //when multiple always return an array of images
                    if (attrs.multiple) scope.image = [];

                    var files = evt.target.files;
                    for (var i = 0; i < files.length; i++) {

                        var imageResult = {
                            file: files[i],
                            url: URL.createObjectURL(files[i])
                        };

                        fileToDataURL(files[i]).then(function(dataURL) {
                            var base64String = dataURL.toString().replace(/data.+base64,/, "");
                            ParseService.createParseFile("photo.jpg", {
                                base64: base64String
                            }, {
                                success: function(imageResult) {
                                    applyScope(imageResult);

                                },
                                error: function(model, error) {
                                    alert('There was a problem retrieving this Headshot.  Please try again later.');

                                }
                            });
                        });

                    }
                });
            }
        };
    })
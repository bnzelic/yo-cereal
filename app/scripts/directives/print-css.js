'use strict';

angular.module('yomomentsMobApp')
    .directive('printcss', function($q, ParseService) {
        'use strict'

        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                composite: '=ngModel'
            },
            template: '<div id="container"></div>',
            link: function postLink(scope, element, attrs, ngModel) {

                function createComposite() {
                    var composite = scope.composite;

                    console.log("Print Composite");

                    if (!composite || typeof composite.headshots === 'undefined') console.log("Data Error");

                    var canvasWidth,
                        canvasHeight,
                        ratio = .8, 
                        i, j, h, w, scale,
                        viewportWidth = document.documentElement.clientWidth,
                        viewportHeight = document.documentElement.clientHeight;

                    scale = viewportWidth * .001;
                    canvasWidth = scale * 1000;
                    canvasHeight = canvasWidth * ratio;
                    console.log("Print | Scale - " + scale + "| Width - " + canvasWidth + "| Height - " + canvasHeight);     
                    
                    

                    var W = canvasWidth, 
                        H = canvasHeight;

                    var headshotSize = composite.moment.get('headshotSize') || 100;
                    var headshotBorderType = composite.moment.get('headshotBorderType') || 'square';
                    var headshotBorderWidth = composite.moment.get('headshotBorderWidth') || 1;
                    var headshotBorderColor = composite.moment.get('headshotBorderColor') || 'white';
                    var headshotNameOn = composite.moment.get('headshotNameOn');
                    var headshotTitleOn = composite.moment.get('headshotTitleOn');
                    var headshotNameFontColor = composite.moment.get('headshotNameFontColor') || 'black';
                    var headshotNameFontType = composite.moment.get('headshotNameFontType') || 'Times';
                    var headshotTitleFontColor = composite.moment.get('headshotTitleFontColor') || 'black';
                    var headshotTitleFontType = composite.moment.get('headshotTitleFontType') || 'Times';
                    var numberOfHeadshots = composite.headshots.length;
                    var headshotNameFontSize = composite.moment.get('headshotNameFontSize') || 20;
                    var headshotTitleFontSize = composite.moment.get('headshotTitleFontSize') || 13;
                    var bgImageOn = composite.moment.get('backgroundImageOn') || false;
                    var bgTile = composite.moment.get('backgroundTile') || false;
                    var bgBorderColor = composite.moment.get('momentBorderColor') || 'white';
                    var bgBorderWidth = composite.moment.get('momentBorderWidth') || 4;
                    var bgImageSrc = composite.moment.get('background');
                    var bgColor = composite.moment.get('backgroundColor') || 'white';
                    var logo_x = composite.moment.get('logo_x') || 300 * scale;
                    var logo_y = composite.moment.get('logo_y') || 200 * scale;
                    var logoScale = composite.moment.get('logoScale') || 1 * scale;
                    var logoBorder = composite.moment.get('logoBorder') || false;
                    var logoBorderColor = composite.moment.get('logoBorderColor') || "white";
                    var logoBorderWidth = composite.moment.get('logoBorderWidth') || 4;
                    var location_x = composite.moment.get('location_x') || 400;
                    var location_y = composite.moment.get('location_y') || 500;
                    var date_x = composite.moment.get('date_x') || 400;
                    var date_y = composite.moment.get('date_y') || 550;
                    var locationOn = composite.moment.get('locationOn');
                    var dateOn = composite.moment.get('dateOn');
                    var location = composite.moment.get('location');
                    var date = composite.moment.get('date');
                    var locationFontSize = composite.moment.get('locationFontSize') || 28;
                    var locationFontColor = composite.moment.get('locationFontColor') || 'black';
                    var locationFontType = composite.moment.get('locationFontType') || 'Times';
                    var dateFontSize = composite.moment.get('dateFontSize') || 28;
                    var dateFontColor = composite.moment.get('dateFontColor') || 'black';
                    var dateFontType = composite.moment.get('dateFontType') || 'Times';
                    var imageRadius = headshotSize / 2 * scale;
                    var borderPadding = (composite.moment.get('momentBorderMargin') || 0) * scale;
                    var headshotShadow = composite.moment.get('headshotShadow') || 'standard';
                    
                    //Get Composite Logo if exists
                    if (composite.moment.get('logo')){
                        var logoSrc = composite.moment.get('logo').url();
                    }
                    
                    bgBorderWidth = bgBorderWidth * scale;

                    
                    //Just Used For Console
                    var grid_width = (W - borderPadding * 2 * 2);
                    console.log("Grid Width - " + grid_width);
                    console.log("Grid Height - " + H);


                    //Create Heashot Sources Array
                    var sources = [];
                   
                    for (var i = 0; i < composite.headshots.length; i++) {
                        //Create Heashot Array with headshot image, name, title 
                        sources.push({
                            imageSrc: composite.headshots[i].get('headshot').url(),
                            name: composite.headshots[i].get('displayName') || '',
                            title: composite.headshots[i].get('title') || '',
                            id: composite.headshots[i].id,
                            x: composite.headshots[i].get('position_x'),
                            y: composite.headshots[i].get('position_y'),
                            moment_Id: composite.moment.id
                        });
                    }

                    function loadImages(sources, callback) {
                        var imageObj;
                        var images = {};
                        var loadedImages = 0;
                        var numImages = sources.length;

                        //Load all the heashots into Image Objects (memory)
                        for (var i = 0; i < numImages; i++) {
                            images[i] = new Image();
                            images[i].onload = function() {
                                if (++loadedImages >= numImages) {
                                    callback(sources, images);
                                }
                            };
                            //Set the source to the url   

                            images[i].src = sources[i].imageSrc;
                        }
                    }

                    //Set the Canvas Size in Kinetic JS
                    var stage = new Kinetic.Stage({
                        container: 'container',
                        width: W,
                        height: H,
                    });

                    //Initialize the Kinetic Layers
                    var backgroundLayer = new Kinetic.Layer();
                    var headshotLayer = new Kinetic.Layer();
                    var logoTitleLayer = new Kinetic.Layer();
                    
                    //Add the Kinetic Layers z-index by order loaded
                    stage.add(backgroundLayer);
                    stage.add(logoTitleLayer);
                    stage.add(headshotLayer);

                    if (bgImageOn) {
                        // create new image object to use as pattern
                        var bgImage = new Image();
                        bgImage.src = bgImageSrc.url();
                        bgImage.onload = function() {
                            if (!bgTile) {

                                var backgroundImage = new Kinetic.Image({
                                    x: 0,
                                    y: 0,
                                    image: bgImage,
                                    width: W,
                                    height: H,
                                    opacity: .6
                                });
                                backgroundLayer.add(backgroundImage);

                                var backgroundBorder = new Kinetic.Rect({
                                    x: 0,
                                    y: 0,
                                    width: W,
                                    height: H,
                                    stroke: bgBorderColor,
                                    strokeWidth: bgBorderWidth,
                                });

                                backgroundLayer.add(backgroundBorder);
                                backgroundLayer.draw();

                            } else {
                                //Initialize Background Color
                                var backgroundImageTile = new Kinetic.Rect({
                                    x: 0,
                                    y: 0,
                                    width: W,
                                    height: H,
                                    fillPatternImage: bgImage,
                                    fillPatternRepeat: 'repeat',
                                    stroke: bgBorderColor,
                                    strokeWidth: bgBorderWidth
                                });

                                //Set Background Tile
                                backgroundLayer.add(backgroundImageTile);
                                backgroundLayer.draw();
                            }

                        }

                    } else {

                        //Initialize Background Color
                        var solidBG = new Kinetic.Rect({
                            x: 0,
                            y: 0,
                            width: W,
                            height: H,
                            fill: bgColor,
                            stroke: bgBorderColor,
                            strokeWidth: bgBorderWidth
                        });

                        //Set Background Color
                        backgroundLayer.add(solidBG);
                        backgroundLayer.draw();

                    }
                    //Initialize the Logo Layer
                    var logo;

                    var imageLogo = new Image();
                    imageLogo.src = logoSrc || ''; //Null if no logo
                    imageLogo.onload = function() {
                        logo = new Kinetic.Image({
                            x: logo_x * scale, //311 * scale,
                            y: logo_y * scale, //250 * scale,
                            image: imageLogo,
                            scale: logoScale * scale,
                            draggable: false
                        });
                        
                        if (locationOn) {
                            //Initialize Composite Title Layer
                            var compositeLocation = new Kinetic.Text({
                                x: location_x * scale,
                                y: location_y * scale,
                                text: location,
                                fontSize: locationFontSize * scale,
                                fontFamily: locationFontType,
                                fill: locationFontColor,
                                align: 'center',
                                draggable: false
                            });

                            logoTitleLayer.add(compositeLocation);

                        }

                        if (dateOn) {
                            //Initialize Composite Date Layer
                            var compositeDate = new Kinetic.Text({
                                x: date_x * scale,
                                y: date_y * scale,
                                text: date,
                                fontSize: dateFontSize * scale,
                                fontFamily: dateFontType,
                                fill: dateFontColor,
                                align: 'center',
                                draggable: false
                            });

                            logoTitleLayer.add(compositeDate);

                        }

                        logoTitleLayer.add(logo);
                        logoTitleLayer.draw();
                    };

                    //Loop through the Image Sources and Load Headshots to Canvas
                    var displayComposite = function(sources, images) {
                        console.log("Looping Through Images");
                        var numImages = sources.length
                        for (var i = 0; i < numImages; i++) {
                            sources[i].x = Math.floor(sources[i].x);
                            sources[i].y = Math.floor(sources[i].y);
                            console.log("Headshot " + i + " - "+ sources[i].name + " | X " + sources[i].x+ " | Y " + sources[i].y);
                            
                            createHeadshot(images[i], sources[i].id, sources[i].name, sources[i].title, sources[i].x, sources[i].y, sources[i].moment_Id);
                        }
                    }
                    //Headshot Objects
                    var createHeadshot = function(imageObj, id, name, title, x, y, moment_Id) {
                        
                        var headshot;
                        var headshotBorder = new Kinetic.Rect;

                        //Headshot Shadow 
                        var shadowOpacity, shadowColor, shadowBlur, shadowOffsetX, shadowOffsetY;
                        if (headshotShadow === 'off') {
                            shadowOpacity =  0,
                            shadowColor = '',
                            shadowBlur = 0,
                            shadowOffsetX = 0,
                            shadowOffsetY = 0;
                        } else if (headshotShadow === 'standard') {
                            shadowOpacity =  0.5,
                            shadowColor = 'black',
                            shadowBlur = 1,
                            shadowOffsetX = 5,
                            shadowOffsetY = 3;
                        }

                        //Width of Headshot
                        x = x * scale;
                        y = y * scale;

                        if (headshotBorderType === "square") {

                            headshot = new Kinetic.Rect({
                                id: id,
                                width: headshotSize * scale,
                                height: headshotSize * scale,
                                fillPatternImage: imageObj,
                                fillPatternRepeat: 'no-repeat',
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });

                        } else if (headshotBorderType === "oval") {

                            headshot = new Kinetic.Ellipse({
                                x: imageRadius,
                                y: imageRadius,
                                id: id,
                                radius: {
                                    x: imageRadius * .8,
                                    y: imageRadius
                                },
                                fillPatternImage: imageObj,
                                fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                fillPatternRepeat: 'no-repeat',
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });

                        } else {
                            headshot = new Kinetic.Circle({
                                x: imageRadius,
                                y: imageRadius,
                                id: id,
                                radius: imageRadius,
                                fillPatternImage: imageObj,
                                fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                fillPatternRepeat: 'no-repeat',
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });
                        };

                        var snapX, snapY;

                       var headshotGroup = new Kinetic.Group({
                        x: x, // - (headshotSize * scale) / 2,
                        y: y, // - (headshotSize * scale) / 2,
                        draggable: false,
                        });

                        if (headshotNameOn) {
                            var nameText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + 5, // + imageRadius * 1.1, //variable depending on headshot size (radius - minus 15)
                                text: name,
                                fontSize: headshotNameFontSize * scale,
                                fontFamily: headshotNameFontType,
                                fill: headshotNameFontColor,
                                width: imageRadius * 4,
                                align: 'center'
                            });

                            nameText.setOffset({
                                x: nameText.getWidth() / 2
                            });
                            headshotGroup.add(nameText);
                        }

                        if (headshotTitleOn) {
                            var titleText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + headshotNameFontSize * scale + 5, // + imageRadius * 1.1, //variable depending on headshot size (radius - minus 15)
                                text: title,
                                fontSize: headshotTitleFontSize * scale,
                                fontFamily: headshotTitleFontType,
                                fill: headshotTitleFontColor,
                                width: imageRadius * 4,
                                align: 'center'
                            });

                            titleText.setOffset({
                                x: titleText.getWidth() / 2
                            });
                            headshotGroup.add(titleText);
                        }

                        //Display the Headshot Circle, Name, Title
                        headshotGroup.add(headshot);
                        headshotGroup.add(headshotBorder);
                        headshotLayer.add(headshotGroup);
                        headshotLayer.draw();
                    }
                    loadImages(sources, displayComposite);
                }
                scope.$watch('composite.moment', createComposite);
            }
        };
    })
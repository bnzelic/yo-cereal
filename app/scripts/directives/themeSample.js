'use strict';

angular.module('yomomentsMobApp')
    .directive('themeSample', function($q) {
        'use strict'

        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                composite: '=ngModel'
            },
            template: '<div id="container"></div>',
            link: function postLink(scope, element, attrs, ngModel) {
				//JRP: moved these outide of function to allow for persistence
				//Set the Canvas Size in Kinetic JS
				var stage = new Kinetic.Stage({
					container: 'container',
				});
				
				var imageAssets = {};

				//Initialize the Kinetic Layers
				var backgroundLayer = new Kinetic.Layer({id:'bgLayer'});
				var headshotLayer = new Kinetic.Layer();
				var logoTitleLayer = new Kinetic.Layer();
				
				function getViewportDimensions(_scale, ratio) { 
                    var viewportWidth = document.documentElement.clientWidth,
                        viewportHeight = document.documentElement.clientHeight;
					
					if(_scale == 3) {
						viewportWidth = _scale * 1000;
						viewportHeight = viewportWidth * ratio;
					}
					else {
						//Desktop
						if (!device.tablet()) {
							if (viewportWidth > 1170) {
								_scale = .9;
								viewportWidth = _scale * 1000;
								viewportHeight = viewportWidth * ratio;
							}
							else if (viewportWidth > 750 && viewportWidth < 1170) {
								_scale = .72;
								viewportWidth = _scale * 1000;
								viewportHeight = viewportWidth * ratio;
							}
							else {
								_scale = viewportWidth * .001;
								viewportWidth = _scale * 1000;
								viewportHeight = viewportWidth * ratio;
							}
						}
						
						//Tablet
						else if (device.tablet()) {
							viewportWidth = viewportWidth - 100;
							_scale = viewportWidth * .001;
							viewportWidth = scale * 1000;
							viewportHeight = viewportWidth * ratio;
						}
					}
					return {w: viewportWidth, h: viewportHeight, scale: _scale};
				}
			
				function createCompositeCanvas() { 
                    var composite = scope.composite;
                    var ratio = .8,
						scale = attrs.scale,
						W = getViewportDimensions(scale, ratio).w, //w * CELL_SIZE,
						H = getViewportDimensions(scale, ratio).h,
						scale = getViewportDimensions(scale, ratio).scale;
					
					imageAssets.headshots = [];
					imageAssets.logo = new Image();
					stage.setWidth(W);
					stage.setHeight(H);
					stage.add(backgroundLayer);
					stage.add(logoTitleLayer);
					stage.add(headshotLayer);
				}
				
				function createBackgroundLayer() { 
					var composite = scope.composite;
					var bgImageOn = composite.moment.get('backgroundImageOn') || false;
					if (!composite || typeof composite.headshots === 'undefined') return;

					if ('local variables') {
                    var ratio = .8;
                    var scale = attrs.scale;
                    
                    var W = stage.getWidth(), //w * CELL_SIZE,
                        H = stage.getHeight(),
						scale = getViewportDimensions(scale, ratio).scale,
                        xSnaps = [],
                        ySnaps = []; //h * CELL_SIZE;
                    
                    var bgTile = composite.moment.get('backgroundTile') || false;
                    var bgBorderColor = composite.moment.get('momentBorderColor') || 'white';
                    var bgBorderWidth = composite.moment.get('momentBorderWidth') * scale || 4 * scale;
                    var bgImageSrc = composite.moment.get('background');
                    var bgColor = composite.moment.get('backgroundColor') || 'white';
                    var logo_x = composite.moment.get('logo_x') || 300 * scale;
                    var logo_y = composite.moment.get('logo_y') || 200 * scale;
                    var borderPadding = (composite.moment.get('momentBorderMargin') || 0) * scale;
					}

					backgroundLayer.removeChildren();
					if (bgImageOn) {
						// create new image object to use as pattern
						var bgImage = new Image();
						bgImage.src = bgImageSrc.url();
						bgImage.onload = function() {
							if (!bgTile) {
								var backgroundImage = new Kinetic.Image({
									x: 0,
									y: 0,
									image: bgImage,
									width: W,
									height: H,
									opacity: .5
								});
								backgroundLayer.add(backgroundImage);

								var backgroundBorder = new Kinetic.Rect({
									x: 0,
									y: 0,
									width: W,
									height: H,
									stroke: bgBorderColor,
									strokeWidth: bgBorderWidth,
								});
						
								backgroundLayer.add(backgroundBorder);
								

							} else {
								//Initialize Background Color
								var backgroundImageTile = new Kinetic.Rect({
									x: 0,
									y: 0,
									width: W,
									height: H,
									fillPatternImage: bgImage,
									fillPatternRepeat: 'repeat',
									stroke: bgBorderColor,
									strokeWidth: bgBorderWidth
								});
						
								backgroundLayer.add(backgroundImageTile);
								
							}
						}
					} else {

						//Initialize Background Color
						var solidBG = new Kinetic.Rect({
							x: 0,
							y: 0,
							width: W,
							height: H,
							fill: bgColor,
							stroke: bgBorderColor,
							strokeWidth: bgBorderWidth
						});
						
						//Set Background Color
						backgroundLayer.add(solidBG);
						
					}
					
				}
				
				function createLogoLayer() { 
					var composite = scope.composite;
					if (!composite || typeof composite.headshots === 'undefined') return;

						if ('Define Local Variables') {
							var ratio = .8;
							var	scale = attrs.scale;
							var W = stage.getWidth(), //w * CELL_SIZE,
								H = stage.getHeight(),
								scale = getViewportDimensions(scale, ratio).scale,
								xSnaps = [],
								ySnaps = []; //h * CELL_SIZE;
								
									  
							var logo_x = composite.moment.get('logo_x') || 300 * scale;
							var logo_y = composite.moment.get('logo_y') || 200 * scale;
							var logoScale = composite.moment.get('logoScale') || 1 * scale;
							var logoBorder = composite.moment.get('logoBorder') || false;
							var logoBorderColor = composite.moment.get('logoBorderColor') || "white";
							var logoBorderWidth = composite.moment.get('logoBorderWidth') || 4;
							var location_x = composite.moment.get('location_x') || 400;
							var location_y = composite.moment.get('location_y') || 500;
							var date_x = composite.moment.get('date_x') || 400;
							var date_y = composite.moment.get('date_y') || 550;
							var locationOn = composite.moment.get('locationOn');
							var dateOn = composite.moment.get('dateOn');
							var location = composite.moment.get('location');
							var date = composite.moment.get('date');
							var locationFontSize = composite.moment.get('locationFontSize') || 28;
							var locationFontColor = composite.moment.get('locationFontColor') || 'black';
							var locationFontType = composite.moment.get('locationFontType') || 'Times';
							var dateFontSize = composite.moment.get('dateFontSize') || 28;
							var dateFontColor = composite.moment.get('dateFontColor') || 'black';
							var dateFontType = composite.moment.get('dateFontType') || 'Times';
							var logoSrc = 'none';
							//Get Composite Logo if exists
							if (composite.moment.get('logo')){
								logoSrc = composite.moment.get('logo').url();
							}
						}

						logoTitleLayer.removeChildren();
						//Initialize the Logo Layer
						
						if (composite.moment.get('logo')) {
							// IF OUR ASSETS HAS CHANGED
							if (imageAssets.logo && imageAssets.logo.src != logoSrc) {
								// RELOAD THE IMAGE ASSET
								//var imageLogo =
								//imageLogo.src = logoSrc || ''; //Null if no logo
								imageAssets.logo.src = logoSrc || '';
								//imageAssets.logo = imageLogo;
								imageAssets.logo.onload = function() { logoTitleLayer.draw(); } 
							}
							
							var logo = new Kinetic.Image({
									x: logo_x * scale, //311 * scale,
									y: logo_y * scale, //250 * scale,
									image: imageAssets.logo,
									scale: logoScale * scale,
									draggable: false
								});
								
							logoTitleLayer.add(logo);
							
							if (logoBorder) {
								logo.setStrokeWidth(logoBorderWidth);
								logo.setStroke(logoBorderColor);
							} else {
								logo.setStrokeWidth(0);
								logo.setStroke("Transparent");
							}
						}

						if (locationOn) {
							//Initialize Composite Title Layer
							var compositeLocation = new Kinetic.Text({
									x: location_x * scale,
									y: location_y * scale,
									text: location,
									fontSize: locationFontSize * scale,
									fontFamily: locationFontType, //'Apple Chancery',
									fill: locationFontColor, //'grey',
									align: 'center',
									draggable: false
								});
							
							logoTitleLayer.add(compositeLocation);
						}

						if (dateOn) {
							//Initialize Composite Time Layer
							var compositeDate = new Kinetic.Text({
								x: date_x * scale,
								y: date_y * scale,
								text: date,
								fontSize: dateFontSize * scale,
								fontFamily: dateFontType, //'Apple Chancery',
								fill: dateFontColor, //'grey',
								align: 'center',
								draggable: false
							});

							logoTitleLayer.add(compositeDate);
						}
					}			
					
				function createHeadshotLayer() { 
					var composite = scope.composite;
					if (!composite || typeof composite.headshots === 'undefined') return;

					if ('local variables') {
                    var ratio = .8;
                    var scale = attrs.scale;
                    
                    var W = stage.getWidth() * scale, //w * CELL_SIZE,
                        H = stage.getHeight()* scale,
						scale = getViewportDimensions(scale, ratio).scale;
						
                    var bgBorderWidth = composite.moment.get('momentBorderWidth') * scale || 4 * scale;
                    var headshotShadow = composite.moment.get('headshotShadow') || 'standard';
					var headshotSize = composite.moment.get('headshotSize') || 100;
					var headshotBorderType = composite.moment.get('headshotBorderType') || 'square';
					var headshotBorderWidth = composite.moment.get('headshotBorderWidth') || 1;
					var headshotBorderColor = composite.moment.get('headshotBorderColor') || 'white';
					var headshotNameOn = composite.moment.get('headshotNameOn');
					var headshotTitleOn = composite.moment.get('headshotTitleOn');
					var headshotNameFontColor = composite.moment.get('headshotNameFontColor') || 'black';
					var headshotNameFontType = composite.moment.get('headshotNameFontType') || 'Times';
					var headshotTitleFontColor = composite.moment.get('headshotTitleFontColor') || 'black';
					var headshotTitleFontType = composite.moment.get('headshotTitleFontType') || 'Times';
					var numberOfHeadshots = composite.headshots.length;
					var headshotNameFontSize = composite.moment.get('headshotNameFontSize') || 20;
					var headshotTitleFontSize = composite.moment.get('headshotTitleFontSize') || 13;
					
				    var xCounter = bgBorderWidth * 2;
                    var yCounter = bgBorderWidth * 2;
                    //Get Composite Logo if exists
                    if (composite.moment.get('logo')){
                        var logoSrc = composite.moment.get('logo').url();
                    }

                    var borderPadding = (composite.moment.get('momentBorderMargin') || 0) * scale;
					var shadowOpacity, shadowColor, shadowBlur, shadowOffsetX, shadowOffsetY;
							if (headshotShadow === 'off') {
								shadowOpacity =  0,
								shadowColor = '',
								shadowBlur = 0,
								shadowOffsetX = 0,
								shadowOffsetY = 0;
								
							} else if (headshotShadow === 'standard') {
								shadowOpacity =  0.5,
								shadowColor = 'black',
								shadowBlur = 10,
								shadowOffsetX = 5 * scale,
								shadowOffsetY = 3 * scale;
							}
					var imageRadius = headshotSize / 2 * scale;		
					}
					
	
					// IF OUR ASSETS COUNT HAS CHANGED
					if (imageAssets.headshots && imageAssets.headshots.length != composite.headshots.length) {
						// RELOAD THE IMAGE ASSETS
						// WOULD PROBABLY BE BETTER TO ONLY LOAD THE ONES THAT CHANGED
						for (var i = 0; i < composite.headshots.length; i++) {
							var imgObj = new Image();
							imgObj.src = composite.headshots[i].get('headshot').url();
							imageAssets.headshots[i] = imgObj;
						}
					}
					
					var headshotGroups = [];

					for (var i = 0; i < composite.headshots.length; i++) {	
						//console.log(composite.headshots[i].get('position_x'));
						//console.log(xCounter);
						var headshotGroup = new Kinetic.Group({
							x: composite.headshots[i].get('position_x') * scale || xCounter, 
							y: composite.headshots[i].get('position_y') * scale || yCounter, 
							draggable: false,
						});
						
						var headshot = null;
						
						if (headshotBorderType === "square") {
						
							headshot = new Kinetic.Rect({
								id: composite.headshots[i].id,
								width: headshotSize * scale,
								height: headshotSize * scale,
								fillPatternImage: imageAssets.headshots[i],	
								fillPatternRepeat: 'no-repeat',
								fillPatternScale: {
									x: imageRadius / 150, //Dependent on original image size (300,300)
									y: imageRadius / 150 //Dependent on original image size (300,300)
								},
								stroke: headshotBorderColor,
								strokeWidth: headshotBorderWidth,
								shadowOpacity: shadowOpacity,
								shadowColor: shadowColor,
								shadowBlur: shadowBlur,
								shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
							  });
							
						} else if (headshotBorderType === "oval") {
							headshot = new Kinetic.Ellipse({
								x: imageRadius,
								y: imageRadius,
								id: composite.headshots[i].id,
								radius: {
									x: imageRadius * .9,
									y: imageRadius
								},
								fillPatternImage: imageAssets.headshots[i],
								fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
								fillPatternScale: {
									x: imageRadius / 150, //Dependent on original image size (300,300)
									y: imageRadius / 150 //Dependent on original image size (300,300)
								},
								fillPatternRepeat: 'no-repeat',
								stroke: headshotBorderColor,
								strokeWidth: headshotBorderWidth,
								shadowOpacity: shadowOpacity,
								shadowColor: shadowColor,
								shadowBlur: shadowBlur,
								shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
							});

						} else {
							headshot = new Kinetic.Circle({
								x: imageRadius,
								y: imageRadius,
								id: composite.headshots[i].id,
								radius: imageRadius,
								fillPatternImage: imageAssets.headshots[i],
								fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
								fillPatternScale: {
									x: imageRadius / 150, //Dependent on original image size (300,300)
									y: imageRadius / 150 //Dependent on original image size (300,300)
								},
								fillPatternRepeat: 'no-repeat',
								stroke: headshotBorderColor,
								strokeWidth: headshotBorderWidth,
								shadowOpacity: shadowOpacity,
								shadowColor: shadowColor,
								shadowBlur: shadowBlur,
								shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
							});
						}
						
						if (headshotNameOn) {
                            var textPadding = 5;
                            if (headshotSize > 100) textPadding = 8;
							var name = composite.headshots[i].get('displayName') || '';
                            var nameText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + textPadding, 
                                text: name,
                                fontSize: headshotNameFontSize * scale,
                                fontFamily: headshotNameFontType,
                                fill: headshotNameFontColor,
                                width: imageRadius * 4,
                                align: 'center'

                            });

                            nameText.setOffset({
                                x: nameText.getWidth() / 2
                            });
                            headshotGroup.add(nameText);
                        }

                        if (headshotTitleOn) {
							var title = composite.headshots[i].get('title') || '';
                            var titleText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + headshotNameFontSize * scale + textPadding,
                                text: title,
                                fontSize: headshotTitleFontSize * scale,
                                fontFamily: headshotTitleFontType,
                                fill: headshotTitleFontColor,
                                width: imageRadius * 4,
                                align: 'center'
                            });

                            titleText.setOffset({
                                x: titleText.getWidth() / 2
                            });
                            headshotGroup.add(titleText);
                        }
						
						headshotGroup.add(headshot);
						headshotGroups.push(headshotGroup);
					}
					
					headshotLayer.removeChildren();
					for (var i = 0; i < headshotGroups.length; ++i) {
						headshotLayer.add(headshotGroups[i]);
						imageAssets.headshots[i].onload = function() { 
							headshotLayer.draw();
						}
					}
				}

			
                function refreshComposite() {
					// var startTime = Date.now();
					// var endTime = Date.now();
					// var bgTime, logoTome, headshottime;
					
                    var composite = scope.composite;
                    //Used for canvas size, headshot location, headshot scale, headshot size, headshot offset

                    if (!composite || typeof composite.headshots === 'undefined') return;
							
							
					createBackgroundLayer();
					//backgroundLayer.draw();
					//bgTime =  Date.now() - startTime;
					
					createLogoLayer();
					//logoTitleLayer.draw();
					//logoTome = Date.now() - bgTime;
					
					createHeadshotLayer();
					//headshotLayer.draw();
					//headshottime = Date.now() - logoTome;
					
					stage.batchDraw();
					
					//endTime = Date.now();
					// console.log('Canvas bgTime in ' + (bgTime) + 'ms');
					//console.log('Canvas logoTome in ' + (logoTome) + 'ms');
					// console.log('Canvas headshottime in ' + (headshottime) + 'ms');
					composite.moment.changed = false;
				// end of return
				}
				
				document.getElementById('save').addEventListener('click', function() {
					/*
					 * since the stage toDataURL() method is asynchronous, we need
					 * to provide a callback
					 */
					stage.toDataURL({
					  callback: function(dataUrl) {
						/*
						 * here you can do anything you like with the data url.
						 * In this tutorial we'll just open the url with the browser
						 * so that you can see the result as an image
						 */
						//window.open(dataUrl);
					  }
					});
				  }, false);
				
				createCompositeCanvas();
				
				scope.$watch('composite.moment',refreshComposite);
				scope.$watch('composite.moment.changed',refreshComposite);
                
            }
        };
    })
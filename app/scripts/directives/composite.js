'use strict';

angular.module('yomomentsMobApp')
    .directive('composite', function($q, ParseService) {
        'use strict'

        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                composite: '=ngModel'
            },
            template: '<div id="container"></div>',
            link: function postLink(scope, element, attrs, ngModel) {

                function createComposite() {
                    console.log("createComposite")
                    var composite = scope.composite;
                    //Used for canvas size, headshot location, headshot scale, headshot size, headshot offset

                    if (!composite || typeof composite.headshots === 'undefined') return;


                    var viewportWidth = document.documentElement.clientWidth,
                        viewportHeight = document.documentElement.clientHeight;

                    var ratio = .8;
                    var i, j, h, w,
                        scale = attrs.scale;
                    console.log(scale);
                if(scale == 3) {
                    viewportWidth = scale * 1000;
                    viewportHeight = viewportWidth * ratio;
                    console.log("Print | Scale - " + scale + "| Width - " + viewportWidth + "| Height - " + viewportHeight);     
                }
                else {
                    //Desktop
                    if (!device.tablet()) {
                        if (viewportWidth > 1170) {
                            //viewportWidth = 900;
                            scale = .9;
                            viewportWidth = scale * 1000;
                            viewportHeight = viewportWidth * ratio;
                            console.log("Desktop | Scale - " + scale + "| Width - " + viewportWidth + "| Height - " + viewportHeight);
                        }
                        else if (viewportWidth > 750 && viewportWidth < 1170) {
                            scale = .72;
                            viewportWidth = scale * 1000;
                            viewportHeight = viewportWidth * ratio;
                            console.log("Desktop | Scale - " + scale + "| Width - " + viewportWidth + "| Height - " + viewportHeight);
                        }
                        else {
                            scale = viewportWidth * .001;
                            viewportWidth = scale * 1000;
                            viewportHeight = viewportWidth * ratio;
                            console.log("Desktop | Scale - " + scale + "| Width - " + viewportWidth + "| Height - " + viewportHeight);
                        }
                    }
                    
                    //Tablet
                    else if (device.tablet()) {
                        viewportWidth = viewportWidth - 100;
                        scale = viewportWidth * .001;
                        viewportWidth = scale * 1000;
                        viewportHeight = viewportWidth * ratio;
                        console.log("Tablet | Scale - " + scale + "| Width - " + viewportWidth + "| Height - " + viewportHeight);

                    }
                }
                    
                    var W = viewportWidth, //w * CELL_SIZE,
                        H = viewportHeight,
                        xSnaps = [],
                        ySnaps = []; //h * CELL_SIZE;

                    var headshotSize = composite.moment.get('headshotSize') || 100;
                    var headshotBorderType = composite.moment.get('headshotBorderType') || 'square';
                    var headshotBorderWidth = composite.moment.get('headshotBorderWidth') || 1;
                    var headshotBorderColor = composite.moment.get('headshotBorderColor') || 'white';
                    var headshotNameOn = composite.moment.get('headshotNameOn');
                    var headshotTitleOn = composite.moment.get('headshotTitleOn');
                    var headshotNameFontColor = composite.moment.get('headshotNameFontColor') || 'black';
                    var headshotNameFontType = composite.moment.get('headshotNameFontType') || 'Times';
                    var headshotTitleFontColor = composite.moment.get('headshotTitleFontColor') || 'black';
                    var headshotTitleFontType = composite.moment.get('headshotTitleFontType') || 'Times';
                    var numberOfHeadshots = composite.headshots.length;
                    var headshotNameFontSize = composite.moment.get('headshotNameFontSize') || 20;
                    var headshotTitleFontSize = composite.moment.get('headshotTitleFontSize') || 13;             
                    var bgImageOn = composite.moment.get('backgroundImageOn') || false;
                    var bgTile = composite.moment.get('backgroundTile') || false;
                    var bgBorderColor = composite.moment.get('momentBorderColor') || 'white';
                    var bgBorderWidth = composite.moment.get('momentBorderWidth') || 4;
                    var bgImageSrc = composite.moment.get('background');
                    var bgColor = composite.moment.get('backgroundColor') || 'white';
                    var logo_x = composite.moment.get('logo_x') || 300 * scale;
                    var logo_y = composite.moment.get('logo_y') || 200 * scale;
                    var logoScale = composite.moment.get('logoScale') || 1 * scale;
                    var logoBorder = composite.moment.get('logoBorder') || false;
                    var logoBorderColor = composite.moment.get('logoBorderColor') || "white";
                    var logoBorderWidth = composite.moment.get('logoBorderWidth') || 4;
                    var location_x = composite.moment.get('location_x') || 400;
                    var location_y = composite.moment.get('location_y') || 500;
                    var date_x = composite.moment.get('date_x') || 400;
                    var date_y = composite.moment.get('date_y') || 550;
                    var locationOn = composite.moment.get('locationOn');
                    var dateOn = composite.moment.get('dateOn');
                    var location = composite.moment.get('location');
                    var date = composite.moment.get('date');
                    var locationFontSize = composite.moment.get('locationFontSize') || 28;
                    var locationFontColor = composite.moment.get('locationFontColor') || 'black';
                    var locationFontType = composite.moment.get('locationFontType') || 'Times';
                    var dateFontSize = composite.moment.get('dateFontSize') || 28;
                    var dateFontColor = composite.moment.get('dateFontColor') || 'black';
                    var dateFontType = composite.moment.get('dateFontType') || 'Times';
                    var headshotShadow = composite.moment.get('headshotShadow') || 'standard';

                    
                    //Get Composite Logo if exists
                    if (composite.moment.get('logo')){
                        var logoSrc = composite.moment.get('logo').url();
                    }

                    var imageRadius = headshotSize / 2 * scale;
                    var borderPadding = (composite.moment.get('momentBorderMargin') || 0) * scale;
                    
                    bgBorderWidth = bgBorderWidth * scale;
                    
                    //Just Used For Console
                    var grid_width = (W - bgBorderWidth * 2 * 2);
                    console.log("Grid Width - " + grid_width);
                    console.log("Grid Height - " + H);
                    console.log("Border Padding - " + borderPadding);
                    console.log("Border Width - " + bgBorderWidth);

                    //Make Grid Height
                    h = Math.floor((H - borderPadding * 2 * 2) / ((headshotSize + headshotNameFontSize + headshotTitleFontSize + 10) * scale));
                    console.log("Number of Rows - " + h);
                    h = (H - borderPadding * 2 * 2) / h;
                    h = Math.floor(h);
                    console.log("Row Height - " + h);
                    
                    //Make Grid Width
                    w = Math.floor((W - borderPadding * 2 * 2) / ((headshotSize + 10) * scale));
                    console.log("Number of Columns - " + w);
                    w = (W - borderPadding * 2 * 2) / w;
                    w = Math.floor(w);
                    console.log("Column Width - " + w);

                    var make_grid = function(layer) {
                        var I = 0;
                        var J = 0;
                        for (i = 0; I < (W - borderPadding * 2) + 1; i++) {
                            I = (i * w) + (borderPadding * 2);
                            xSnaps.push(I - imageRadius);
                            xSnaps.push((I + ((w / 2) - imageRadius)));

                            var l = new Kinetic.Line({
                                stroke: "white",
                                strokeWidth: 1,
                                points: [I, borderPadding * 2, I, H - borderPadding * 2],
                                dashArray: [33, 10]
                            });
                            layer.add(l);
                        }

                        for (j = 0; J < (H - borderPadding * 2) + 1; j++) {
                            J = (j * h) + (borderPadding * 2);
                            ySnaps.push((J + ((h / 2) - imageRadius) - headshotTitleFontSize));
                            var l2 = new Kinetic.Line({
                                stroke: "white",
                                strokeWidth: 1,
                                points: [borderPadding * 2, J, W - borderPadding * 2, J],
                                dashArray: [33, 10]
                            });
                            layer.add(l2);
                        }

                        ySnaps.splice(j - 1, 1);
                        ySnaps.splice(j - 2, 1);
                        //   ySnaps.splice(j-1,1);
                        //  ySnaps.splice(0,1);
                        return layer;
                    };

                    var snapX = function(x) {
                        var newX = x;
                        var maxX = xSnaps[xSnaps.length - 5];
                        var minX = xSnaps[0];
                        if (newX < minX) {
                            newX = minX + w / 2;
                        } else if (newX > maxX) {
                            newX = maxX;
                        } else {
                            for (var i = 0; i < xSnaps.length; i++) {
                                var snapX = xSnaps[i];
                                if (newX <= snapX) {
                                    newX = snapX;
                                    break;
                                }
                            }
                        }

                        return newX;

                    };

                    var snapY = function(y) {
                        var newY = y;
                        var maxY = ySnaps[ySnaps.length - 1] + (h / 2);
                        var minY = ySnaps[0];
                        if (newY < minY) {
                            newY = minY;
                        } else if (newY > ySnaps[ySnaps.length - 1]) {
                            newY = maxY - (h / 2);

                        } else {
                            for (var i = 0; i < ySnaps.length; i++) {
                                var snapY = ySnaps[i] + (h / 2);
                                if (newY <= snapY) {
                                    newY = snapY - (h / 2); //imageRadius - headshotTitleFontSize - (10 * scale);
                                    break;
                                }
                            }
                        }

                        return newY;
                    };


                    //Create Heashot Sources Array
                    var sources = [];
                    var xCounter = bgBorderWidth * 2;
                    var yCounter = bgBorderWidth * 2;

                    for (var i = 0; i < composite.headshots.length; i++) {
                        //Create Heashot Array with headshot image, name, title 
                        sources.push({
                            imageSrc: composite.headshots[i].get('headshot').url(),
                            name: composite.headshots[i].get('displayName') || '',
                            title: composite.headshots[i].get('title') || '',
                            id: composite.headshots[i].id,
                            x: composite.headshots[i].get('position_x') || xCounter,
                            y: composite.headshots[i].get('position_y') || yCounter,
                            moment_Id: composite.moment.id
                            //headshot.get x and y coordinates. Different templates with different x/y's for different composite sizes
                        });
                        //A
                        if (xCounter < (W - w - bgBorderWidth * 2 - 10)) {
                            xCounter = xCounter + w;
                            while (yCounter > (200 - headshotSize) && yCounter < (600 - headshotSize) && xCounter > (300 - headshotSize) && xCounter < 700) {
                                xCounter = xCounter + w;
                            }
                        } else {
                            xCounter = bgBorderWidth * 2;
                            yCounter = yCounter + h;
                        }

                    }

                    function loadImages(sources, callback) {
                        var imageObj;
                        var images = {};
                        var loadedImages = 0;
                        var numImages = sources.length;

                        //Load all the heashots into Image Objects (memory)
                        for (var i = 0; i < numImages; i++) {
                            images[i] = new Image();
                            images[i].onload = function() {
                                if (++loadedImages >= numImages) {
                                    callback(sources, images);
                                }
                            };
                            //Set the source to the url   

                            images[i].src = sources[i].imageSrc;
                        }
                    }

                    //Set the Canvas Size in Kinetic JS
                    var stage = new Kinetic.Stage({
                        container: 'container',
                        width: W,
                        height: H,
                    });

                    //Initialize the Kinetic Layers
                    var backgroundLayer = new Kinetic.Layer();
                    var headshotLayer = new Kinetic.Layer();
                    var logoTitleLayer = new Kinetic.Layer();
                    var gridLayer = new Kinetic.Layer();
                    var gr = make_grid(gridLayer);

                    gridLayer.draw();
                    gridLayer.hide();
                    //Add the Kinetic Layers z-index by order loaded
                    stage.add(backgroundLayer);
                    stage.add(gridLayer);
                    stage.add(logoTitleLayer);
                    stage.add(headshotLayer);
                    

                    //Background Section
                    if (bgImageOn) {
                        // create new image object to use as pattern

                        var bgImage = new Image();
                        bgImage.src = bgImageSrc.url();
                        bgImage.onload = function() {
                            if (!bgTile) {

                                var backgroundImage = new Kinetic.Image({
                                    x: 0,
                                    y: 0,
                                    image: bgImage,
                                    width: W,
                                    height: H,
                                    opacity: .5
                                });
                                backgroundLayer.add(backgroundImage);

                                var backgroundBorder = new Kinetic.Rect({
                                    x: 0,
                                    y: 0,
                                    width: W,
                                    height: H,
                                    stroke: bgBorderColor,
                                    strokeWidth: bgBorderWidth,
                                });
                        
                                backgroundLayer.add(backgroundBorder);
                                backgroundLayer.draw();

                            } else {
                                //Initialize Background Color
                                var backgroundImageTile = new Kinetic.Rect({
                                    x: 0,
                                    y: 0,
                                    width: W,
                                    height: H,
                                    fillPatternImage: bgImage,
                                    fillPatternRepeat: 'repeat',
                                    stroke: bgBorderColor,
                                    strokeWidth: bgBorderWidth
                                });
                        
                                backgroundLayer.add(backgroundImageTile);
                                backgroundLayer.draw();
                            }

                        }

                    } else {

                        //Initialize Background Color
                        var solidBG = new Kinetic.Rect({
                            x: 0,
                            y: 0,
                            width: W,
                            height: H,
                            fill: bgColor,
                            stroke: bgBorderColor,
                            strokeWidth: bgBorderWidth
                        });
                        
                        //Set Background Color
                        backgroundLayer.add(solidBG);
                        backgroundLayer.draw();

                    }
                    //Initialize the Logo Layer
                    var logo;

                    var imageLogo = new Image();
                    imageLogo.src = logoSrc || ''; //Null if no logo
                    imageLogo.onload = function() {
                        logo = new Kinetic.Image({
                            x: logo_x * scale, //311 * scale,
                            y: logo_y * scale, //250 * scale,
                            image: imageLogo,
                            scale: logoScale * scale,
                            draggable: true
                        });
                        if (logoBorder) {
                            logo.setStrokeWidth(logoBorderWidth);
                            logo.setStroke(logoBorderColor);
                        } else {
                            logo.setStrokeWidth(0);
                            logo.setStroke("Transparent");

                        }

                        if (locationOn) {
                            //Initialize Composite Title Layer
                            var compositeLocation = new Kinetic.Text({
                                x: location_x * scale,
                                y: location_y * scale,
                                text: location,
                                fontSize: locationFontSize * scale,
                                fontFamily: locationFontType, //'Apple Chancery',
                                fill: locationFontColor, //'grey',
                                align: 'center',
                                draggable: true
                            });

                            compositeLocation.on('dragstart', function() {
                                this.moveToTop();
                                this.setOpacity(0.50);
                                // rect.show();
                                // headshotLayer.draw();
                            });

                            compositeLocation.on("dragend", function() {
                                this.setOpacity(1.00);
                                ParseService.setLocationXY(composite.moment.id, compositeLocation.getAbsolutePosition().x / scale, compositeLocation.getAbsolutePosition().y / scale, {
                                    success: function(user) {

                                    },
                                    error: function(user, error) {
                                        alert('Invalid xy.');
                                    }
                                });
                            });

                            // add cursor styling
                            compositeLocation.on("mouseover", function() {
                                document.body.style.cursor = "pointer";
                            });
                            compositeLocation.on("mouseout", function() {
                                document.body.style.cursor = "default";
                            });

                            logoTitleLayer.add(compositeLocation);

                        }

                        if (dateOn) {
                            //Initialize Composite Time Layer
                            var compositeDate = new Kinetic.Text({
                                x: date_x * scale,
                                y: date_y * scale,
                                text: date,
                                fontSize: dateFontSize * scale,
                                fontFamily: dateFontType, //'Apple Chancery',
                                fill: dateFontColor, //'grey',
                                align: 'center',
                                draggable: true
                            });

                            compositeDate.on('dragstart', function() {
                                this.moveToTop();
                                this.setOpacity(0.50);
                                // rect.show();
                                // headshotLayer.draw();
                            });

                            compositeDate.on("dragend", function() {
                                this.setOpacity(1.00);
                                ParseService.setDateXY(composite.moment.id, compositeDate.getAbsolutePosition().x / scale, compositeDate.getAbsolutePosition().y / scale, {
                                    success: function(user) {

                                    },
                                    error: function(user, error) {
                                        alert('Invalid xy.');
                                    }
                                });
                            });

                            compositeDate.on("mouseover", function() {
                                document.body.style.cursor = "pointer";
                            });
                            compositeDate.on("mouseout", function() {
                                document.body.style.cursor = "default";
                            });

                            logoTitleLayer.add(compositeDate);

                        }

                        logo.on('dragstart', function() {
                            this.moveToTop();
                            this.setOpacity(0.50);
                            if (!logoBorder) {
                                logo.setStrokeWidth(logoBorderWidth);
                                logo.setStroke(logoBorderColor);
                                logoTitleLayer.draw();
                            }

                        });

                        logo.on("dragend", function() {
                            this.setOpacity(1.00);
                            if (!logoBorder) {
                                logo.setStrokeWidth(0);
                                logo.setStroke("Transparent");
                                logoTitleLayer.draw();
                            }

                            ParseService.setLogoXY(composite.moment.id, logo.getAbsolutePosition().x / scale, logo.getAbsolutePosition().y / scale, {
                                success: function(user) {

                                },
                                error: function(user, error) {
                                    alert('Invalid xy.');
                                }
                            });
                        });

                        // add cursor styling
                        logo.on("mouseover", function() {
                            document.body.style.cursor = "pointer";
                        });
                        logo.on("mouseout", function() {
                            document.body.style.cursor = "default";
                        });

                        logoTitleLayer.add(logo);
                        logoTitleLayer.draw();
                    };

                    //Loop through the Image Sources and Load Headshots to Canvas
                    var displayComposite = function(sources, images) {
                        console.log("Looping Through Images...");
                        var numImages = sources.length
                        for (var i = 0; i < numImages; i++) {
                            createHeadshot(images[i], sources[i].id, sources[i].name, sources[i].title, sources[i].x, sources[i].y, sources[i].moment_Id);
                        }
                    }

                    var createHeadshot = function(imageObj, id, name, title, x, y, moment_Id) {
                        var headshot;
                        var headshotBorder = new Kinetic.Rect;

                        //Headshot Shadow 
                        var shadowOpacity, shadowColor, shadowBlur, shadowOffsetX, shadowOffsetY;
                        if (headshotShadow === 'off') {
                            shadowOpacity =  0,
                            shadowColor = '',
                            shadowBlur = 0,
                            shadowOffsetX = 0,
                            shadowOffsetY = 0;
                        } else if (headshotShadow === 'standard') {
                            shadowOpacity =  0.5,
                            shadowColor = 'black',
                            shadowBlur = 10,
                            shadowOffsetX = 5 * scale,
                            shadowOffsetY = 3 * scale;
                        }
                        
                        //Position of Headshot
                        x = x * scale;
                        y = y * scale;

                        if (headshotBorderType === "square") {

                            headshot = new Kinetic.Rect({
                                id: id,
                                width: headshotSize * scale,
                                height: headshotSize * scale,
                                fillPatternImage: imageObj,
                                fillPatternRepeat: 'no-repeat',
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });

                        } else if (headshotBorderType === "oval") {

                            headshot = new Kinetic.Ellipse({
                                x: imageRadius,
                                y: imageRadius,
                                id: id,
                                radius: {
                                    x: imageRadius * .9,
                                    y: imageRadius
                                },
                                fillPatternImage: imageObj,
                                fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                fillPatternRepeat: 'no-repeat',
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });

                        } else {
                            headshot = new Kinetic.Circle({
                                x: imageRadius,
                                y: imageRadius,
                                id: id,
                                radius: imageRadius,
                                fillPatternImage: imageObj,
                                fillPatternOffset: [150, 150], //Dependent on original image size 300x300 -> offset 150,150
                                fillPatternScale: {
                                    x: imageRadius / 150, //Dependent on original image size (300,300)
                                    y: imageRadius / 150 //Dependent on original image size (300,300)
                                },
                                fillPatternRepeat: 'no-repeat',
                                stroke: headshotBorderColor,
                                strokeWidth: headshotBorderWidth,
                                shadowOpacity: shadowOpacity,
                                shadowColor: shadowColor,
                                shadowBlur: shadowBlur,
                                shadowOffset: {x:shadowOffsetX, y:shadowOffsetY}
                            });
                        };

                        var headshotGroup = new Kinetic.Group({
                            x: x, 
                            y: y, 
                            draggable: true,
                            dragBoundFunc: function(pos) {
                                return {
                                    x: snapX(pos.x),
                                    y: snapY(pos.y)
                                }
                            },
                        });

                        if (headshotNameOn) {
                            var textPadding = 5;
                            if (headshotSize > 100) textPadding = 8;
                            var nameText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + textPadding, 
                                text: name,
                                fontSize: headshotNameFontSize * scale,
                                fontFamily: headshotNameFontType,
                                fill: headshotNameFontColor,
                                width: imageRadius * 4,
                                align: 'center'

                            });

                            nameText.setOffset({
                                x: nameText.getWidth() / 2
                            });
                            headshotGroup.add(nameText);
                        }

                        if (headshotTitleOn) {
                            var titleText = new Kinetic.Text({
                                x: (headshotSize / 2) * scale,
                                y: (headshotSize) * scale + headshotNameFontSize * scale + textPadding,
                                text: title,
                                fontSize: headshotTitleFontSize * scale,
                                fontFamily: headshotTitleFontType,
                                fill: headshotTitleFontColor,
                                width: imageRadius * 4,
                                align: 'center'
                            });

                            titleText.setOffset({
                                x: titleText.getWidth() / 2
                            });
                            headshotGroup.add(titleText);
                        }

                        //Display the Headshot, Name, Title
                        headshotGroup.add(headshot);
                        headshotGroup.add(headshotBorder);
                        headshotLayer.add(headshotGroup);
                        headshotLayer.draw();

                        // Add Cursor Styling
                        headshotGroup.on("mouseover", function() {
                            document.body.style.cursor = "pointer";
                        });
                        headshotGroup.on("mouseout", function() {
                            document.body.style.cursor = "default";
                        });

                        headshotGroup.on('dragstart', function() {
                            this.moveToTop();
                            this.setOpacity(0.50);
                            headshotBorder.show();
                            gridLayer.show();
                            headshotLayer.draw();
                        });

                        headshotGroup.on("dragend", function() {
                            this.setOpacity(1.00);
                            gridLayer.hide();
                            headshotBorder.hide();
                            var x, y;

                            //Remove Decimals From New Positions
                            x = Math.floor(headshotGroup.getAbsolutePosition().x / scale);
                            y = Math.floor(headshotGroup.getAbsolutePosition().y / scale);

                            //Save New Coordinates
                            ParseService.setHeadshotXY(headshot.getId(), x, y);
                            
                        });
                    }
                    loadImages(sources, displayComposite);
                    document.getElementById('save').addEventListener('click', function() {
        /*
         * since the stage toDataURL() method is asynchronous, we need
         * to provide a callback
         */
        stage.toDataURL({
          callback: function(dataUrl) {
            /*
             * here you can do anything you like with the data url.
             * In this tutorial we'll just open the url with the browser
             * so that you can see the result as an image
             */
            window.open(dataUrl);
          }
        });
      }, false);   
                }
                scope.$watch('composite.moment', createComposite);           
            }
        };
    })
'use strict';

/* Services */

angular.module('yomomentsServices', ['ngResource'])
  .factory('ParseCereal', function($resource, $q) {
    /*----------------------------------------------------------------------------------------------------------
    BZ - WHY DOES THIS EXIST? We didn't want to write to Shawn's database and instead wrote to our own Parse Databse. 
         However we didn't want to recreate Shawn's database from scratch and are now instead utalizing both
    ----------------------------------------------------------------------------------------------------------
    */  

    //toggle wether we're using our or shawns database
    var toggleParse = function(setCereal) {
      if (setCereal) {
        Parse.initialize("E6uqKoSEZHCIOdSGA23jBsRR8ZSqVq5e3xXadOsI", "vYoFCuMSzhg1uH1A48MR3AuBtLTm0b9IbJPpx74i");
      } else {
        Parse.initialize("thjzNbpKx34GIRmkCeUs6uS1HvjgG3fZ1PCtDuIb", "Oj3RAph3KpjTaP9mL0SJZP7zsAm8AV7Bj0D9kAPd");
      }
    };

    //Create Layout
    var loadAllLayouts = function(options) {
      toggleParse(true);
      var deferred = $q.defer();
      var Layout = Parse.Object.extend("Layout");
      var query = new Parse.Query(Layout);

      query.find({
        success: function(layouts) {
          toggleParse(false);
          deferred.resolve(layouts);
        },
        error: function(object, error) {
          console.log("loadLayout error:" + error);
          toggleParse(false);
          alert('There was a problem getting the layouts')
        }
      });

      return deferred.promise;
    };

    var createLayout = function(data) {
        var deferred = $q.defer();
        toggleParse(true);
        var Layout = Parse.Object.extend("Layout");
        var layout = new Layout();
        layout.set('title', data.join('-'));
        layout.set('data', data);

        layout.save(null, {
            success: function(object) {
                toggleParse(false);
                deferred.resolve(object);
            },
            error: function(model, error) {
                toggleParse(false);
                alert('There was a problem saving the layout.')
            }
        });

        return deferred.promise;
    };

    var updateLayout = function(newLayout) {
      toggleParse(true); //write to our database
      var deferred = $q.defer();

      var Layout = Parse.Object.extend("Layout");
      var layout = new Layout();

      layout.id = newLayout.id;
      layout.set('title', newLayout.attributes.data.join('-'));
      layout.set('data',  newLayout.attributes.data);

      layout.save(null, {
        success: function(object) {
          toggleParse(false);
          deferred.resolve(object);
        },
        error: function() {
          toggleParse(false);
          alert('There was a problem saving the layout')
        }
      });
      return deferred.promise;
    };

    var deleteLayout = function (_id) {
      var deferred = $q.defer();
      toggleParse(true);
      var Layout = Parse.Object.extend("Layout");
      var layout = new Layout();
      layout.id = _id;
      layout.destroy({
        success: function(object) {
          toggleParse(false);
          deferred.resolve(object);
        },
        error: function() {
          toggleParse(false);
          alert('There was a problem deleting the layout.')
        }
      });

      return deferred.promise;
    };

    // Create a new theme
    var createTheme = function (_title, _data, options) {
      toggleParse(true);
      var Theme = Parse.Object.extend("Themes");
      var theme = new Theme();
	
	  _data.themeTitle = _title;
	 
	  theme.set("themeTitle", _title);
      theme.set("data",  _data);


      theme.save(null, {
        success: function(object) {
          toggleParse(false);
		  if (options.success) {
            options.success(theme);
          }
        },
        error: function(model, error) {
          toggleParse(false);
          if (options.error) {
            options.error();
          }
        }
      });
    }

    var loadAllThemes = function(options) {
      toggleParse(true);
      var Theme = Parse.Object.extend("Themes");
      var query = new Parse.Query(Theme);

      query.find({
          success: function(themes) {
            toggleParse(false);
            if (options.success) {
              options.success(themes);
            }
          },
          error: function(object, error) {
            console.log("loadTheme error:" + error);
            toggleParse(false);
            if (options.error) {
              options.error();
            }
          }
      });
    };

        //basically a more generalized version of applyTheme
        //with the correct name however. This process might be expensive however.
        var updateComposite = function(_id, theme, headshots, options) {
            toggleParse(false); //write to shawn's db
            var Moment = Parse.Object.extend("Moments");

            var moment = new Moment();
            moment.id = _id;

            for (var attr in theme.moment.attributes) {
                moment.set(attr, theme.moment.attributes[attr]);
            }

            //TODO: headshots has no 'set' method
            for (var i = 0; i < headshots.length; i++) {

                for (var attr in headshots.at(i).attributes) {
                    headshots.at(i).set(attr, headshots.models[i].attributes[attr]);
                }
            }

            moment.save(null, {
                success: function(object) {
                    Parse.Object.saveAll(headshots, {
                        success: function(objs) {
                            toggleParse(false);
                            if (options.success) {
                                options.success(moment);
                            }
                        }
                    });

                },
                error: function(object, error) {
                    toggleParse(false);
                    if (options.error) {
                        options.error();
                    }
                }
            })
        }

    var applyTheme = function(_id, theme, options) {
      toggleParse(false); //write to shawn's db
      var Moment = Parse.Object.extend("Moments");

      var moment = new Moment();
      moment.id = _id;

      for (var attr in theme) {
        moment.set(attr, theme[attr]);
      }

	  
      moment.save(null, {
        success: function(object) {
          toggleParse(false);
          if (options.success) {
            options.success(moment);
          }
        },
          error: function(object, error) {
			//console.log(object);
			console.log(error);
            toggleParse(false);
            if (options.error) {
              options.error();
            }
          }
      })
    }

    var editTheme = function(_id, data, options) {
      toggleParse(true); //write to our database

      var Theme = Parse.Object.extend("Themes");

      var theme = new Theme();
      theme.id = _id;

      theme.set('data', data);

      theme.save(null, {
        success: function(object) {
          toggleParse(false); 
          if (options.success) {
            options.success(theme);
          }
        },
          error: function(object, error) {
            toggleParse(false); 
            if (options.error) {
              options.error();
            }
          }
      })
    }

    var getTheme = function(_id, options) {
      toggleParse(true);
      var Theme = Parse.Object.extend("Themes");
      var theme = new Parse.Query(Theme);
      var currentTheme = {};

      theme.get(_id).then(function(currentTheme) {
        //console.log(currentTheme);
        return currentTheme.get('data');
      }).then(function(data) {
		//console.log(data);
        toggleParse(false); 
        if (options.success) {
          options.success(data);
        }
      }, function(error) {
        toggleParse(false);
        if (options.error) {
          options.error();
        }
      });
    }


      var deleteTheme = function (_id, options) {
        toggleParse(true);
        var Theme = Parse.Object.extend("Themes");
        var theme = new Theme();
        theme.id = _id;
        theme.destroy({
          success: function(object) {
            toggleParse(false);
            options.success(theme);
          },
          error: function(object, error) {
            toggleParse(false);
            if (options.error) {
              options.error();
            }
          }
        });
      }

    return { 
        name: 'ParseCereal',
        createTheme: createTheme,
        loadAllThemes: loadAllThemes,
        applyTheme: applyTheme,
        updateComposite: updateComposite,
        editTheme: editTheme,
        getTheme: getTheme,
        deleteTheme: deleteTheme,
        createLayout: createLayout,
        loadAllLayouts: loadAllLayouts,
        deleteLayout: deleteLayout,
        updateLayout: updateLayout
      }

  })

  .factory('ParseService', function($resource) {
    // Initialize Parse API and objects.
    Parse.initialize("thjzNbpKx34GIRmkCeUs6uS1HvjgG3fZ1PCtDuIb", "Oj3RAph3KpjTaP9mL0SJZP7zsAm8AV7Bj0D9kAPd");

    // Add FB javascript

  /*  window.fbAsyncInit = function() {
      // init the FB JS SDK
      Parse.FacebookUtils.init({
        appId: '225510680935582', // App ID from the app dashboard
        channelUrl: '//alpha.yomo.us/channel.html', // Channel file for x-domain comms
        status: false, // Check Facebook Login status
        cookie: true, // enable cookies to allow Parse to access the session
        xfbml: true // Look for social plugins on the page
      });


      // Additional initialization code such as adding Event Listeners goes here
    };

    // Load the SDK asynchronously
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

*/
    // Cache current logged in user
    var loggedInUser;
    var currentMoment;
    var currentHeadshot;

    // Define parse model and collection for classes
    var Moment = Parse.Object.extend("Moments");
    var Headshot = Parse.Object.extend("Headshots");
    var Group = Parse.Object.extend("Groups");
    var Category = Parse.Object.extend("Categories");
    var Logo = Parse.Object.extend("Logos");

    // ParseService Object
    var ParseService = {
      name: "Parse",

      // Login a user
      login: function login(username, password, options) {
        Parse.User.logIn(username, password, {
          success: function(user) {
            loggedInUser = user;
            if (options.success) {
              options.success(user);
            }
          },
          error: function(user, error) {
            alert("Error: " + error.message);
            if (options.error) {
              options.error();
            }
          }
        });
      },

      // Logout a user
      logout: function logout() {
        Parse.User.logOut();
        loggedInUser = null;
      },

      // Reset Password
      resetPassword: function resetPassword(email, options) {
        Parse.User.requestPasswordReset(email, {
          success: function() {
            // Password reset request was sent successfully
            if (options.success) {
              options.success();
            }
          },
          error: function(error) {
            // Show the error message somewhere
            alert("Error: " + error.code + " " + error.message);
            if (options.error) {
              options.error();
            }
          }
        });

      },

      // Login a user using Facebook
      FB_login: function FB_login(options) {
        Parse.FacebookUtils.logIn("user_likes,email", {
          success: function(user) {
            if (!user.existed()) {
              alert("User signed up and logged in through Facebook!");
              FB.api('/me?fields=name,email,link,picture.type(large)', function(response) {
                if (!response.error) {

                  //console.log("response.picture.data.url =" + response.picture.data.url);
                  Parse.Cloud.run('httpRequest', {
                    url: response.picture.data.url
                  }, {
                    success: function(result) {

                      user.set("avatar", result);
                      user.save(null, {
                        success: function(user) {
                          //do anything after save
                        },
                        error: function(user, error) {
                          console.log("Oops, something went wrong saving your name.");

                          if (options.error) {
                            options.error();
                          }
                        }
                      });
                      console.log("parseFile saved.");
                    },
                    error: function(error) {
                      console.log("parse.html - the file either could not be read, or could not be saved to Parse.");
                      // The file either could not be read, or could not be saved to Parse.
                    }
                  });

                  user.set("fullName", response.name);
                  user.set("email", response.email);
                  user.set("username", response.email);
                  user.set("facebook", response.link);
                  user.set("ACL", new Parse.ACL(Parse.User.current()));

                  user.save(null, {
                    success: function(user) {
                      // do anything need after save
                      loggedInUser = user;
                      if (options.success) {
                        options.success(user);
                      }

                    },
                    error: function(user, error) {
                      if (options.error) {
                        options.error();
                      }
                      console.log("Oops, something went wrong saving your name.");
                    }
                  });
                } else {
                  if (options.error) {
                    options.error();
                  }
                  console.log("Oops something went wrong with facebook.");
                }
              });
            } else {
              //alert("Facebook Login Successfull!");
              loggedInUser = user;
              if (options.success) {
                options.success(user);
              }

            }

          },
          error: function(user, error) {
            if (options.error) {
              options.error();
            }
            alert("User cancelled the Facebook login or did not fully authorize.");
          }
        });
      },

      // Update a user
      updateUser: function updateUser(user, options) {

        user.save(null, {
          success: function(user) {

            loggedInUser = user;
            if (options.success) {
              options.success(user);
            }

            // Execute any logic that should take place after the object is saved.
            alert('Updated user with Id: ' + user.id);
          },
          error: function(user, error) {
            // Execute any logic that should take place if the save fails.
            // error is a Parse.Error with an error code and description.
            if (options.error) {
              options.error();
            }
            alert('Failed to update user, with error code: ' + error.description);
          }
        });

      },

      // Register a user
      signUp: function signUp(username, password, fullName, options) {
        Parse.User.signUp(username, password, {
          fullName: fullName,
          ACL: new Parse.ACL()
        }, {
          success: function(user) {
            loggedInUser = user;
            if (options.success) {
              options.success(user);
            }
          },

          error: function(user, error) {
            alert("Error: " + error.message);
            if (options.error) {
              options.error();
            }

          }
        });
      },

      getMoment: function getMoment(id, options) {
        var query = new Parse.Query(Moment);
        query.get(id, {
          success: function(moment) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }
          },
          error: function(object, error) {
            console.log(error);
            //console.log(object);
            console.log("getMoment error:" + error);
            if (options.error) {
              options.error();
            }
          }
        });

      },

      getCurrentMoment: function getCurrentMoment() {

        return currentMoment;
      },

      getHeadshot: function getHeadshot(id, options) {

        var query = new Parse.Query(Headshot);
        query.include("associatedUser");
        query.include("moments");
        query.get(id, {
          success: function(headshot) {
            //console.log(headshot);
            currentHeadshot = headshot;

            if (options.success) {
              options.success(headshot);
            }
          },
          error: function(headshot, error) {
            console.log("getMoment error:" + error);
            if (options.error) {
              options.error();
            }
          }
        });

      },

      getNewHeadshot: function getNewHeadshot() { return new Headshot(); },
      getNewMoment: function getNewMoment() { return new Moment(); },

      getMoments: function getMoments(options) {

        Parse.Cloud.run('getAllMoments', {}, {
          success: function(result) {
            //console.log(result);
            if (options.success) {
              options.success(result);
            }
          },
          error: function(error) {
            console.log("getMoments error:" + error);
            if (options.error) {
              options.error();
            }
          }
        });
      },

      getHeadshots: function getHeadshots(options) {

        Parse.Cloud.run('getAllHeadshots', {}, {
          success: function(result) {
            //console.log(result);
            if (options.success) {
              options.success(result);
            }
          },
          error: function(error) {
            console.log("getHeadshots error:" + error);
            if (options.error) {
              options.error();
            }
          }
        });
      },

      resetHeadshotXY: function resetHeadshotXY(momentId, options) {

        var Collection = Parse.Collection.extend({
          model: Headshot,
          query: (new Parse.Query(Headshot)).equalTo('moments', currentMoment || momentId)
        });
        var collection = new Collection();
        collection.fetch({
          success: function(result) {
            //console.log(result);

            // this will store the rows for use with Parse.Object.saveAll
            var headshotArray = [];

            for (var i = 0; i < result.length; i++) {
              var headshot = result.at(i);
              headshot.unset('position_x');
              headshot.unset('position_y');
              headshotArray.push(headshot);

            };

            // save all the newly created objects
            Parse.Object.saveAll(headshotArray, {
              success: function(objs) {

                var moment = new Moment();
                moment.id = momentId;
                moment.unset('logo_x');
                moment.unset('logo_y');
                moment.unset('location_x');
                moment.unset('location_y');
                moment.unset('date_x');
                moment.unset('date_y');
                moment.save({
                  success: function(object) {
                    currentMoment = object;
                    if (options.success) {
                      options.success(object);
                    }

                  },
                  error: function(object, error) {
                    if (options.error) {
                      options.error();
                    }
                  }
                });

              },
              error: function(error) {
                if (options.error) {
                  options.error();
                }
                console.error("resetHeadshotXY - update failed: " + error);
              }
            });

          },
          error: function(result, error) {
            if (options.error) {
              options.error();
            }
            console.error("resetHeadshotXY - fetch failed: " + error);
          }
        });
      },

      getHeadshotsInMoment: function getHeadshotsInMoment(options, moment) {
        var Collection = Parse.Collection.extend({
          model: Headshot,
          query: (new Parse.Query(Headshot)).equalTo('moments', currentMoment || moment)
        });
        var collection = new Collection();
        collection.fetch({
          success: function(result) {
            // console.log(result);
            if (options.success) {
              options.success(result);
            }
          },
          error: function(result, error) {

            console.error("getHeadshotsInMoment - fetch failed: " + error);
            if (options.error) {
              options.error();
            }
          }
        });
      },

      getHeadshotsInMomentFeed: function getHeadshotsInMoment(options, moment) {
        var Collection = Parse.Collection.extend({
          model: Headshot,
          query: (new Parse.Query(Headshot)).equalTo('moments', moment)
        });
        var collection = new Collection();
        collection.fetch({
          success: function(result) {
            //console.log(result);
            if (options.success) {
              options.success(result);
            }
          },
          error: function(result, error) {

            console.error("getHeadshotsInMoment - fetch failed: " + error);
            if (options.error) {
              options.error();
            }
          }
        });
      },

      // Create a new moment
      createMoment: function createMoment(_title, _location, _date, options) {
        var moment = new Moment();

        moment.save({
          title: _title,
          location: _location,
          date: _date,
          admin: Parse.User.current()
        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(model, error) {
            if (options.error) {
              options.error();
            }
          }
        });
      }, //createMoment

      createParseFile: function createParseFile(_fileName, _file, options) {

        var parseFile = new Parse.File(_fileName, _file);
        parseFile.save().then(function() {
          // The file has been saved to Parse.
          if (options.success) {
            options.success(parseFile);
          }
        }, function(error) {
          // The file either could not be read, or could not be saved to Parse.
          if (options.error) {
            options.error();
          }
        });
      },
      adminCreateHeadshot: function adminCreateHeadshot(_id, _displayName, _title, _websiteID, _emailID, _file, _moment, options) {

        var headshot = new Headshot();
        headshot.id = _id;
        headshot.save({
          displayName: _displayName,
          title: _title,
          websiteID: _websiteID,
          emailID: _emailID,
          moments: _moment || currentMoment,
          createdBy: Parse.User.current(), //make Admin user at some point
          photo: _file/*,
          ACL: new Parse.ACL(Parse.User.current())*/
        }, {
          success: function(object) {
            currentHeadshot = headshot;
            if (options.success) {
              options.success(headshot);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      updateLogo: function updateLogo(_id, _file, options) {
        var moment = new Moment();
        moment.id = _id;
        moment.save({
          logo: _file
        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      updateInfo: function updateLogo(_id, _title, _date, location, options) {
        var moment = new Moment();
        moment.id = _id;
        moment.save({
          title: _title,
          date: _date,
          location: _location

        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      updateHeadshot: function updateHeadshot(_id, _displayName, _title, _photo, options) {
        var headshot = new Headshot();
        headshot.id = _id;
        headshot.save(
{          displayName: _displayName,
          title: _title,
          photo: _photo
        }, {
          success: function(object) {
            if (options.success) {
              options.success(headshot);
            }
          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        
        });
      },

      // Create a new Headshot Record
      createHeadshot: function createHeadshot(_id, _displayName, _title, _shareEmail, _shareFacebook, _shareTwitter, _file, options) {

        var headshot = new Headshot();
        headshot.id = _id;
        headshot.save({
          displayName: _displayName,
          title: _title,
          shareEmail: _shareEmail,
          shareFacebook: _shareFacebook,
          shareTwitter: _shareTwitter,
          moments: currentMoment,
          createdBy: Parse.User.current(),
          photo: _file/*,
          ACL: new Parse.ACL(Parse.User.current())*/
        }, {
          success: function(object) {
            currentHeadshot = headshot;
            if (options.success) {
              options.success(headshot);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },

      // // Create a new Headshot Record
      // updateHeadshotPhoto: function createHeadshot(_id, ,_file, options) {

      //   var headshot = new Headshot();
      //   headshot.id = _id;
      //   headshot.save({
      //     photo: _file/*,
      //     ACL: new Parse.ACL(Parse.User.current())*/
      //   }, {
      //     success: function(object) {
      //       currentHeadshot = headshot;
      //       if (options.success) {
      //         options.success(headshot);
      //       }

      //     },
      //     error: function(object, error) {
      //       if (options.error) {
      //         options.error();
      //       }
      //     }
      //   });

      // },
      // Create a new Headshot Record
      deleteHeadshot: function createHeadshot(_id, options) {

        var headshot = new Headshot();
        headshot.id = _id;
        headshot.destroy({
          success: function(object) {
            currentHeadshot = headshot;
            if (options.success) {
              options.success(headshot);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },

      deleteMoment: function deleteMoment(_id, options) {
        
        var moment = new Moment();
        moment.id = _id;
        moment.destroy({
          success: function(object) {
            options.success(moment);
          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },

      //New Service to Update Individual Attributes of Moment Class
      updateMoment1: function updateMoment1(_id, _attribute, _value, options) {
        var moment = new Moment();
        moment.id = _id;
        var attr = _attribute; //Set Object Column to Update
        //console.log(attr, "set to", _value);
        moment.set(attr, _value);
        moment.save(null, {
          success: function(moment) {
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(moment, error) {
            if (options.error) {
              options.error();
            }
          }
        });
      },


      // OLD METHOD FOR UPDATING MOMENT
      updateMoment: function updateMoment(_id,
        _backgroundcolor,
        _background,
        _backgroundImageOn,
        _backgroundTile,
        _logoScale,
        _location,
        _date,
        _headshotsize,
        _locationOn,
        _locationFontColor,
        _locationFontSize,
        _locationFontType,
        _dateOn,
        _dateFontColor,
        _dateFontSize,
        _dateFontType,
        _headshotNameOn,
        _headshotNameFontColor,
        _headshotNameFontSize,
        _headshotNameFontType,
        _headshotTitleOn,
        _headshotTitleFontColor,
        _headshotTitleFontSize,
        _headshotTitleFontType,
        _logo,
        _logoBorder,
        _logoBorderColor,
        _logoBorderWidth,
        _headshotBorderType,
        _headshotBorderWidth,
        _headshotBorderColor,
        _momentBorderWidth,
        _momentBorderColor,
        _momentBorderMargin,
        _grid_w,
        _grid_h,
        options) {

        var moment = new Moment();
        moment.id = _id;
        moment.save({
          backgroundColor: _backgroundcolor,
          background: _background,
          backgroundImageOn: _backgroundImageOn,
          backgroundTile: _backgroundTile,
          logoScale: _logoScale,
          location: _location,
          date: _date,
          headshotSize: _headshotsize,
          locationOn: _locationOn,
          locationFontColor: _locationFontColor,
          locationFontSize: _locationFontSize,
          locationFontType: _locationFontType,
          dateOn: _dateOn,
          dateFontColor: _dateFontColor,
          dateFontSize: _dateFontSize,
          dateFontType: _dateFontType,
          headshotNameOn: _headshotNameOn,
          headshotNameFontColor: _headshotNameFontColor,
          headshotNameFontSize: _headshotNameFontSize,
          headshotNameFontType: _headshotNameFontType,
          headshotTitleOn: _headshotTitleOn,
          headshotTitleFontColor: _headshotTitleFontColor,
          headshotTitleFontSize: _headshotTitleFontSize,
          headshotTitleFontType: _headshotTitleFontType,
          logo: _logo,
          logoBorder: _logoBorder,
          logoBorderColor: _logoBorderColor,
          logoBorderWidth: _logoBorderWidth,
          headshotBorderType: _headshotBorderType,
          headshotBorderWidth: _headshotBorderWidth,
          headshotBorderColor: _headshotBorderColor,
          momentBorderWidth: _momentBorderWidth,
          momentBorderColor: _momentBorderColor,
          momentBorderMargin: _momentBorderMargin,
          grid_w: _grid_w,
          grid_h: _grid_h
        }, {
          success: function(moment) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(moment, error) {
            if (options.error) {
              options.error();
            }
          }
        });
      },

      // Create a new Headshot Record
      setLogoXY: function setLogoXY(_id, _x, _y, options) {

        var moment = new Moment();
        moment.id = _id;
        moment.save({
          logo_x: _x,
          logo_y: _y
        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      // Create a new Headshot Record
      setLocationXY: function setLocationXY(_id, _x, _y, options) {

        var moment = new Moment();
        moment.id = _id;
        moment.save({
          location_x: _x,
          location_y: _y
        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      // Create a new Headshot Record
      setDateXY: function setDateXY(_id, _x, _y, options) {

        var moment = new Moment();
        moment.id = _id;
        moment.save({
          date_x: _x,
          date_y: _y
        }, {
          success: function(object) {
            currentMoment = moment;
            if (options.success) {
              options.success(moment);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },

      // Create a new Headshot Record
      setYomoLogoXY: function setYomoLogoXY(_id, _x, _y, options) {


        var logo = new Logo();
        logo.id = _id;
        logo.save({
          x: _x,
          y: _y

        }, {
          success: function(object) {
            if (options.success) {
              options.success(object);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },

      // Create a new Headshot Record
      setHeadshotXY: function createHeadshot(_id, _x, _y, options) {

        var headshot = new Headshot();
        headshot.id = _id;
        headshot.save({
          position_x: _x,
          position_y: _y
        }, {
          success: function(object) {
            currentHeadshot = headshot;
            if (options.success) {
              options.success(headshot);
            }

          },
          error: function(object, error) {
            if (options.error) {
              options.error();
            }
          }
        });

      },
      // Get current logged in user
      getUser: function getUser() {
        // if (loggedInUser) {
        //   return loggedInUser;
        // }
        return Parse.User.current();
      },

      createGroup: function createGroup(_name, _shortName, _category, _type, options) {

        var group = new Group();

        group.save({
          name: _name,
          shortName: _shortName,
          type: _type,
          category: _category/*,
          ACL: new Parse.ACL(Parse.User.current())*/
        }, {
          success: function(object) {
            if (options.success) {
              options.success(group);
            }
          },
          error: function(model, error) {
            if (options.error) {
              options.error();
            }
          }
        });
      }

    };

    // The factory function returns ParseService, which is injected into controllers.
    return ParseService;
  })
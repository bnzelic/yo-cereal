
angular.module('yomomentsMobApp').factory('mvLayout', function($q, ParseCereal) {



    var center = function(width, size, num) {
        // if width = 900. You'd expect an X value of 450 to center it right? Wrong
        // depending on the the head-shot size, the canvas behaves differently
        // so we adjust for that. This makes no sense to me, so treat this as a temp workaround.
        // ex: archer layout behaves as if it's an 800px canvas width.
        if (size > 150) {
            width = width - (size/2);
        }
        //subtract 20 because of padding
        return width/2 - (.5 * size) - (num - 2)*(size/2) - ((num -1) *10);
    };
	
	//finds the maximum number of shots in the last row to make a triangle
	var  getTriangleMaxRow = function (numberOfHeadshots) {
			var number_x = 1;
			while((number_x*(number_x+1)/2) < numberOfHeadshots) number_x++;
			
			return number_x;
	};

    return {
        setLayout1: function(composite, headshots) {
            var deferred = $q.defer();
            var attr = {
                width:  document.getElementById('container').offsetWidth,
                height:  document.getElementById('container').offsetHeight
            };
            var headshotSize = composite.moment.attributes.headshotSize;
            var headshotsLen = headshots.length;
            var maxPerRow = Math.floor(attr.width/(headshotSize + 20));
            var startingPosx = center(attr.width,headshotSize,maxPerRow); //
            var posx = startingPosx;
            var posy = composite.moment.attributes.momentBorderWidth + 20 || 20;
            var rows = Math.ceil(headshotsLen/5);
            var headNum = 0;
            var totalHeadNum = maxPerRow;
            var lastRow = false;

            for (var i = 0; i < rows; i++) {
                posx = startingPosx; //reset each row

                for (headNum; headNum <totalHeadNum; headNum++) {
                    if (headNum === headshots.length) break;
                    if (totalHeadNum > headshots.models.length) { //center the rest
                        if (!lastRow) {
                            posx = center(attr.width,headshotSize,(maxPerRow - (totalHeadNum - headshots.models.length)));
                        }
                        lastRow = true;
                    }
                    headshots.models[headNum].attributes.position_x = posx;
                    headshots.models[headNum].attributes.position_y = posy;
                    console.log({posx: posx, posy: posy});
                    posx += headshotSize + 20;
                }

                totalHeadNum += maxPerRow;
                posy += headshotSize + 60;
            }



            console.log(headshots);

            // apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });

            return deferred.promise;
        },
		 
		
		
		alignHeadshotsAsTriangleUp: function(composite, headshots) {
			var deferred = $q.defer();
            var canvasWidth = document.getElementById('container').offsetWidth;
			var canvasHeight = document.getElementById('container').offsetWidth;
                
            var headshotWidth = composite.moment.attributes.headshotSize;
			var headshotHeight = composite.moment.attributes.headshotSize;
            var numberOfHeadshots = headshots.length;
            
			var baseRowCount = getTriangleMaxRow(numberOfHeadshots);
			var borderWidth = composite.moment.attributes.momentBorderWidth || 20;
			var borderMargin = composite.moment.attributes.momentBorderMargin || 20;
			var posx;
			var posy = canvasHeight - headshotHeight*2-borderWidth-borderMargin;
			var headshotNum = 0;
			for(var i = baseRowCount; i > 0; i--) { 
				posx = center(canvasWidth, headshotWidth, i);
				var lastHeadshotInRow = i+headshotNum;
				
				for (headshotNum; headshotNum < lastHeadshotInRow; headshotNum++) { 
					if (headshotNum >= numberOfHeadshots) break;
					
					//console.log(headshots.models[headshotNum]);
					headshots.models[headshotNum].attributes.position_x = posx;
					headshots.models[headshotNum].attributes.position_y = posy;
					posx += headshotWidth + 20;
				}
				posy -= headshotHeight +60;
			}
			
			// apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });

            return deferred.promise;
		},
		
		alignHeadshotsAsTriangleDown: function(composite, headshots) {
			var deferred = $q.defer();
            var canvasWidth = document.getElementById('container').offsetWidth;
			var canvasHeight = document.getElementById('container').offsetWidth;
                
            var headshotWidth = composite.moment.attributes.headshotSize;
			var headshotHeight = composite.moment.attributes.headshotSize;
            var numberOfHeadshots = headshots.length;
            
			var baseRowCount = getTriangleMaxRow(numberOfHeadshots);
			var posx;
			var posy = composite.moment.attributes.momentBorderWidth || 20;
			var headshotNum = 0;
			for(var i = baseRowCount; i > 0; i--) { 
				posx = center(canvasWidth, headshotWidth, i);
				var lastHeadshotInRow = i+headshotNum;
				
				for (headshotNum; headshotNum < lastHeadshotInRow; headshotNum++) { 
					if (headshotNum >= numberOfHeadshots) break;
					
					//console.log(headshots.models[headshotNum]);
					headshots.models[headshotNum].attributes.position_x = posx;
					headshots.models[headshotNum].attributes.position_y = posy;
					posx += headshotWidth + 20;
				}
				posy += headshotHeight +60;
			}
			
			// apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });

            return deferred.promise;
		},
		
		alignHeadshotsAsDiamond: function(composite, headshots) {
			var deferred = $q.defer();
            var canvasWidth = document.getElementById('container').offsetWidth;
			var canvasHeight = document.getElementById('container').offsetWidth;
                
            var headshotWidth = composite.moment.attributes.headshotSize;
			var headshotHeight = composite.moment.attributes.headshotSize;
            var numberOfHeadshots = headshots.length;
            
			var baseRowCount = getTriangleMaxRow(numberOfHeadshots);
			var borderWidth = composite.moment.attributes.momentBorderWidth || 20;
			var borderMargin = composite.moment.attributes.momentBorderMargin || 20;
			var posx;
			var posy = canvasHeight - headshotHeight*2-borderWidth-borderMargin;
			var headshotNum = 0;
			for(var i = baseRowCount; i > 0; i--) { 
				posx = center(canvasWidth, headshotWidth, i);
				var lastHeadshotInRow = i+headshotNum;
				
				for (headshotNum; headshotNum < lastHeadshotInRow; headshotNum++) { 
					if (headshotNum >= numberOfHeadshots) break;
					
					//console.log(headshots.models[headshotNum]);
					headshots.models[headshotNum].attributes.position_x = posx;
					headshots.models[headshotNum].attributes.position_y = posy;
					posx += headshotWidth + 20;
				}
				posy -= headshotHeight +60;
			}
			
			// apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });

            return deferred.promise;
		},
		
		alignHeadshotsInCircle: function(composite, headshots) {
			var deferred = $q.defer();
            var canvasWidth = document.getElementById('container').offsetWidth;
			var canvasHeight = document.getElementById('container').offsetWidth;
                
            var headshotWidth = composite.moment.attributes.headshotSize;
			var headshotHeight = composite.moment.attributes.headshotSize + composite.moment.attributes.headshotNameFontSize + composite.moment.attributes.headshotTitleFontSize;
            var numberOfHeadshots = headshots.length;
            
			var borderWidth = composite.moment.attributes.momentBorderWidth || 20;
			var borderMargin = composite.moment.attributes.momentBorderMargin || 20;
			
			var radius = (canvasWidth - borderMargin*2)/2;
			
			var posx = canvasWidth/2-radius+headshotWidth/2;
			var posy = canvasHeight - headshotHeight*2-borderWidth-borderMargin;
			var headshotNum = 0;
			
			var headshotsInSection = numberOfHeadshots/4;
			var degreesPerHeadshot = 360/numberOfHeadshots;
			console.log(degreesPerHeadshot);
			
			for (var i = 0; i < numberOfHeadshots; i++) {
				posx = Math.cos(degreesPerHeadshot*i*Math.PI/180);
				posy = Math.sin(degreesPerHeadshot*i*Math.PI/180);
				console.log('('+posx*radius+' , '+ posy*radius+')');
				//console.log(headshots);
				headshots.models[i].attributes.position_x = posx*radius*0.6+Math.ceil(canvasWidth/2);
				headshots.models[i].attributes.position_y = posy*radius*0.6+canvasHeight/2-headshotHeight;
			} 
			
			// apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });
		
            return deferred.promise;
		},
		
        //rowArray is the number of headshots per row. Up to 10 rows.
        loadLayout: function(composite, headshots, rowArray) {
            if (!rowArray) {
                rowArray = [5,4,3,2,1,2,3,4,5];
            }
            var attr = {
                width:  document.getElementById('container').offsetWidth,
                height:  document.getElementById('container').offsetHeight
            };
            var headshotSize = composite.moment.attributes.headshotSize;
            var deferred = $q.defer();
            var maxPerRow = rowArray[0];
            var posx;
            var posy = composite.moment.attributes.momentBorderWidth + 20 || 20;
            var rows = rowArray.length;
            var headNum = 0;
            var totalHeadNum = maxPerRow;

            for (var i = 0; i < rows; i++) {
                posx = center(attr.width,headshotSize,Math.min(rowArray[i], headshots.length - headNum)); //reset each row

                for (headNum; headNum <totalHeadNum; headNum++) {
                    if (headNum === headshots.length) break;

                    headshots.models[headNum].attributes.position_x = posx;
                    headshots.models[headNum].attributes.position_y = posy;
                    console.log({posx: posx, posy: posy});
                    posx += headshotSize + 20;
                }

                totalHeadNum += rowArray[i+1];
                posy += headshotSize + 60;
            }

            // apply theme to composite
            ParseCereal.updateComposite(composite.moment.id, composite, headshots, {
                success: function(obj) {
                    deferred.resolve();
                },
                error: function() {
                    alert('There was a problem applying the layout.')
                }
            });

            return deferred.promise;
        }
    }
});
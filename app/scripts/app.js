'use strict';

  var app =  angular.module('yomomentsMobApp', ['ngRoute', 'ngAnimate', 'imageupload', 'yomomentsServices', 'ui.bootstrap', 'yomomentsFilters']);
  app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        title: 'YoMo | Home'
      })
      .when('/signup', {
        templateUrl: 'views/partials/signup.html',
        controller: 'LoginCtrl',
        title: 'YoMo | Create Account'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'FeedCtrl',
        title: 'YoMo | Feed'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        title: 'YoMo | Profile'
      })
      .when('/profile/settings', {
        templateUrl: 'views/partials/settings.html',
        controller: 'ProfileCtrl',
        title: 'YoMo | Settings'
      })
      .when('/headshot', {
        templateUrl: 'views/headshot.html',
        controller: 'HeadshotAddCtrl',
        title: 'YoMo'
      })
      .when('/composite/:momentId', {
        templateUrl: 'views/composite.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/print/:momentId', {
        templateUrl: 'views/composite-print.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/printhd/:momentId', {
        templateUrl: 'views/composite-print-hd.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/printcss/:momentId', {
        templateUrl: 'views/composite-print-css.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/composite-sq/:momentId', {
        templateUrl: 'views/composite-sq.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/edit/:momentId', {
        templateUrl: 'views/edit-moment.html',
        controller: 'MomentEditCtrl',
        title: 'YoMo'
      })
      .when('/edit2/:momentId', {
        templateUrl: 'views/edit.html',
        controller: 'MomentEditCtrl',
        title: 'YoMo'
      })
      .when('/headshot/:headshotId', {
        templateUrl: 'views/headshot.html',
        controller: 'MomentEditCtrl',
        title: 'YoMo'
      })
      .when('/moment/:momentId', {
        templateUrl: 'views/moment.html',
        controller: 'MomentCtrl',
        title: 'YoMo'
      })
      .when('/moment/:vanity', {
        templateUrl: 'views/moment.html',
        controller: 'MomentCtrl',
        title: 'YoMo'
      })
      .when('/composite2/:momentId', {
        templateUrl: 'views/composite-sq.html',
        controller: 'CompositeCreateCtrl',
        title: 'YoMo'
      })
      .when('/themes', {
        templateUrl: 'views/theme-list.html',
        controller: 'ThemeCtrl',
        title: 'YoMo - Theme List'
      })
      .when('/themes/:themeId', {
        templateUrl: 'views/theme-edit.html',
        controller: 'ThemeCtrl',
        title: 'YoMo - Theme Edit'
      })
      .when('/layouts', {
        templateUrl: 'views/layout.html',
        controller: 'LayoutCtrl',
        title: 'YoMo - Layout'
      })
    
  });

  //Set Page Title
  app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
  }]);


/**
 * Created by bojan on 4/19/14.
 */
angular.module('yomomentsFilters', []).filter('layoutFilter', function() {
  return function(filters, headshotLen) {
    var newFilters = [];
    var total = 0;

    if (filters) {
      for(var i = 0; i < filters.length; i++) {
        total = 0;
        for (var j = 0; j < filters[i].attributes.data.length; j++) {
          total += filters[i].attributes.data[j];
        }
        if (headshotLen <= total) {
          newFilters.push(filters[i]);
        }
      }
    }
    return newFilters;
  };
});
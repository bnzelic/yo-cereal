'use strict';

angular.module('yomomentsMobApp').controller('FeedCtrl', function($location, $scope, ParseService) {
  
  $scope.init = function() {
    $scope.getMoments();
  }; 

  $scope.getMoments = function() {
    //Get Current User Id
    var currentUser = Parse.User.current();
    var userId = currentUser.id;

    ParseService.getMoments({
      success: function(moments) {
        $scope.$apply(function() {
          $scope.moments = moments.models;
        });
        
        $.each($scope.moments, function(idx, moment) {
             ParseService.getHeadshotsInMomentFeed({
               success: function(headshots) {
                 $scope.$apply(function() {
                   moment.headshotsInFeed = headshots.toArray();
                   });
               },
               error: function(headshot, error) {
                 //alert('There was a problem getting these headshots.  Please try again later.');
               }
             }, moment);

            //Get Admin of Moment Id
            var momentAdmin = moment.get('admin').fetch({
              success: function(admin) {
                if (admin.id === userId) {
                  $scope.$apply(function() {
                    moment.editable = true;
                  });
                }
                else {
                  $scope.$apply(function() {
                    moment.editable = false;
                  });
                }
              },
              error: function(error) {
                console.log("Error Retrieving Moment Admin");
              }
            });

        });

      }, //success
      error: function(moments) {
        alert('Error getting moments');
      }

    }); //getMoments

  };
    

$scope.init();

}); //FeedCtrl
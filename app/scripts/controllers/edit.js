'use strict';

angular.module('yomomentsMobApp')
  .controller('MomentEditCtrl', function($scope, $route, $location, $routeParams, ParseService) {
   
    //Initializer
    $scope.init = function() {

      //Check If user Logged In & Log Out If Not
      if (!ParseService.getUser()) {
        $location.path('/');
      }
      
      $scope.headshot = ParseService.getNewHeadshot();
        
      //Routing for /edit/:momentid
      if ($routeParams.momentId) {
        ParseService.getMoment($routeParams.momentId, {
          success: function(moment) {
            ParseService.getHeadshotsInMoment({
              success: function(headshots) {
                $scope.$apply(function() {
                  $scope.headshots = headshots.toArray();
                  $scope.currentMoment = moment;
                  console.log($scope.currentMoment.id);
                });
                console.log(moment);
              },
              error: function(headshot, error) {
                alert('Something Went Wrong Here');
              }

            }, moment);
          },
          error: function(moments) {
            alert('Something Went Wrong Here');
          }

        }); //End Parse Service
      } 
    }

    $scope.init();

    $scope.$watch('currentMoment', function(newValue) {
      ParseService.getHeadshotsInMoment({
        success: function(headshots) {
          $scope.$apply(function() {
            $scope.headshots = headshots.toArray();
          });
        },
        error: function(headshot, error) {
          alert('Something went wrong getting headshots');
        }

      }, newValue);

    });

    //Add Headshot Function
    $scope.addHeadshot = function() {
      var headshot = $scope.headshot.attributes;
      var moment = $scope.currentMoment;
      console.log(headshot.displayName);
      //  console.log(headshot.title);

      ParseService.adminCreateHeadshot($scope.headshot.id, headshot.displayName, headshot.title, headshot.websiteID, headshot.emailID, headshot.photo, moment, {
        success: function(object) {
          $location.path("/edit/" + object.attributes.moments.id);
          $scope.init();
        },
        error: function(model, error) {
          alert('Something went wrong adding this headshot');
        }
      });

    }

    //Update Logo Function
    $scope.changeLogo = false;
    $scope.toggleChangeLogo = function() {
            $scope.changeLogo = $scope.changeLogo === false ? true: false;
    };

    $scope.updateLogo = function() {
      var moment = $scope.currentMoment;
      /*console.log(moment);*/

      ParseService.updateLogo(moment.id, moment.logo, {
        success: function(object) {
    
          $scope.changeLogo = false;
          $scope.init();
        },
        error: function(model, error) {
          alert('There was a problem uploading logo');
        }
      });

    }

    //Update Info Function
    $scope.changeInfo = false;
    $scope.toggleChangeInfo = function() {
            $scope.changeInfo = $scope.changeInfo === false ? true: false;
        };

    $scope.updateInfo = function() {
      var moment = $scope.currentMoment;

      ParseService.updateInfo(moment.id, moment.newTitle, moment.newDate, moment.newLocation, {
        success: function(object) {
          $scope.changeInfo = false;
          $scope.init();
        },
        error: function(model, error) {
          alert('There was a problem uploading info');
        }
      });

    }

    //Update Headshot Function
    $scope.changeHeadshot = false;
    $scope.toggleUpdateHeadshot = function() {
            $scope.changeHeadshot = $scope.changeHeadshot === false ? true: false;
        };

    $scope.updateHeadshot = function() {
      var headshotToChange = $scope.selectedHeadshot;

      ParseService.updateHeadshot(headshotToChange.id, headshotToChange.attributes.displayName, headshotToChange.attributes.title, headshotToChange.attributes.photo, {
        success: function(object) {
          
          $('#editHeadshot').modal('hide');
          $scope.init();
        }
      });
    }

    //Select Headshot Function
    $scope.selectHeadshot = function(headshot) {
      ParseService.getHeadshot(headshot.id, {
        success: function(object) {
          $scope.$apply(function() {
            $scope.selectedHeadshot = object;
          });
        },
        error: function(model, error) {
          alert('error getting headshot');
        }

      }); //getHeadshot service

    }
    
    $scope.deleteHeadshot = function(headshot) {
      ParseService.deleteHeadshot(headshot.id, {
        success: function(object) {
          $location.path("/edit/" + $scope.currentMoment.id );
          $scope.init();
        },
        error: function(model, error) {
          alert('Something went wrong deleting this headshot');
        }
      });
    }

    $scope.deleteMoment = function(moment2) {
      ParseService.getMoment($routeParams.momentId, {
        success: function(moment) {
          console.log('Deleting Momoent' + moment.id);
          ParseService.deleteMoment(moment.id, {
            success: function(object) {
              $location.path('/home');
              $scope.$apply();
            },
            error: function(model, error) {
              alert('Something went wrong deleting this moment');
            }
          }); //end Parse Service
            
    }

  });
      
      

}


 var featherEditor = new Aviary.Feather({
       apiKey: ' cdeaa873aef1b3d4',
       apiVersion: 3.1,
       theme: 'minimum', 
       tools: 'all',
       maxSize: 800, 
       displayImageSize: true, 
       appendTo: 'injection_site',
       onSave: function(imageID, newURL) {
           var img = document.getElementById(imageID);
           img.src = newURL;

        Parse.Cloud.run('httpRequest', {
                    url: encodeURI(newURL)
                  }, {
                    success: function(result) {
                    $scope.selectedHeadshot.attributes.photo = result;
                    featherEditor.close();
                      console.log("parseFile saved.");
                    },
                    error: function(error) {
                      console.log("parse.html - the file either could not be read, or could not be saved to Parse.");
                      // The file either could not be read, or could not be saved to Parse.
                    }
                  });
       },
       onError: function(errorObj) {
           alert(errorObj.message);
       }
   });


    $scope.launchEditor = function(id) {
        featherEditor.launch({
            image: id,
            url: $scope.selectedHeadshot.attributes.photo.url()
        });
        return false;
    };


    $scope.setMoment = function() {
      var headshot = $scope.attributes;
      var moment = $scope.currentMoment;

      console.log(headshot.displayName);
      console.log(headshot.title);

    }

  });
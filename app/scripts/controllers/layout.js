'use strict';

angular.module('yomomentsMobApp')
  .controller('LayoutCtrl', function($scope, ParseCereal) {

    $scope.rows = [0];
    $scope.layouts = undefined;
    //Keeps track if we're deleting a layout or editing a layout
    $scope.currentLayout = false;

    $scope.addRow = function () {
      $scope.rows.push("");
    };

    $scope.applyRow = function ($index, value) {
      $scope.rows[$index] = value;
    };

    $scope.deleteRow = function (index) {
      $scope.rows.splice(index, 1);
    };

    $scope.saveLayout = function () {
      if ($scope.currentLayout) {
        //editing an existing layout
        ParseCereal.updateLayout($scope.currentLayout)
          .then($scope.init)
      } else {
        ParseCereal.createLayout($scope.rows)
          .then($scope.init)
      }
    };

    $scope.loadAllLayouts = function() {
      ParseCereal.loadAllLayouts().then(function(layouts) {
          $scope.layouts = layouts;
        })
    };

    $scope.deleteLayout = function(layout) {
      ParseCereal.deleteLayout(layout.id)
        .then(function(deleted) {
          $scope.init();
        })
    };

    $scope.editLayout = function(layout) {
      $scope.currentLayout = layout;
      $scope.rows = layout.attributes.data;
    };

    $scope.newLayout = function() {
      $scope.currentLayout = false;
      $scope.rows = [0];
    };

    $scope.init = function() {
      $scope.loadAllLayouts();
    };

    $scope.init();
  });
  

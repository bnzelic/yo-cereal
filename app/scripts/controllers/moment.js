'use strict';

angular.module('yomomentsMobApp')
  .controller('MomentCtrl', function($location, $scope, $routeParams, ParseService) {
 
    $scope.init = function() {

      //Create Heashot Sources Array
      var sources = [];
      var logo = [];

     
/*       for (var i = 0; i < headshots.length; i++) {
          //Create Heashot Array with headshot image, name, title 
          sources.push({
              imageSrc: headshots[i].get('headshot').url(),
              name: headshots[i].get('displayName') || '',
              title: headshots[i].get('title') || '',
              id: headshots[i].id,
              x: headshots[i].get('position_x'),
              y: headshots[i].get('position_y'),
              moment_Id: moment.id
          });
      }

      function loadImages(sources, callback) {
          var imageObj;
          var images = {};
          var loadedImages = 0;
          var numImages = sources.length;

          //Load all the heashots into Image Objects (memory)
          for (var i = 0; i < numImages; i++) {
              images[i] = new Image();
              images[i].onload = function() {
                  if (++loadedImages >= numImages) {
                      callback(sources, images);
                  }
              };
              //Set the source to the url

              images[i].src = sources[i].imageSrc;
          }
      }
*/  $scope.getMoment();
    //console.log($scope.moment);

    };
        //Select Headshot Function
   //Update Logo Function
    $scope.leaveMsg = false;
    $scope.dropLine = function() {
            $scope.leaveMsg = $scope.leaveMsg === false ? true: false;
    };

    $scope.updateMsg = function() {
      /*console.log(moment);*/

      ParseService.leaveMsg(headshot.id, moment.logo, {
        success: function(object) {
    
          $scope.changeLogo = false;
          $scope.init();
        },
        error: function(model, error) {
          alert('There was a problem uploading logo');
        }
      });

    }

    $scope.getMoment = function() {
      ParseService.getMoment($routeParams.momentId, {
        success: function(moment) {
          ParseService.getHeadshotsInMoment({
            success: function(headshots) {
              $scope.$apply(function() {
                $scope.headshots = headshots.toArray();
                $scope.moment = moment; 
                var momentScale = .333333333;
                console.log(momentScale);
                var logo_x = Math.floor(moment.attributes.logo_x);
                var logo_y = Math.floor(moment.attributes.logo_y);
                var logo_scale = Math.abs(moment.attributes.logoScale);
                console.log("Logo Scale: " + logo_scale + " X:" + logo_x + " Y:" + logo_y);
                var logo_height, logo_width;
                var logo_src = moment.get('logo').url();

                function findHandW() {
                  logo_height = Math.floor(this.height*momentScale);
                  logo_width = Math.floor(this.width*momentScale);
                  console.log(logo_height);
                  $scope.logoH = logo_height;
                  $scope.logoW = logo_width;
                }
                function showImage(imgPath) {
                  var myImage = new Image();
                  myImage.name = imgPath;
                  myImage.onload = findHandW;
                  myImage.src = imgPath;
                }

                showImage(logo_src);
                $scope.logoX = logo_x * momentScale;
                $scope.logoY = logo_y * momentScale;

              });
            },
            error: function(headshot, error) {
              alert('There was a problem getting these headshots.  Please try again later.');
            }

          });


        },
        error: function(moment, error) {
          /*alert('There was a problem getting this moment.  Please try again later.');*/
        }

      });

    };


    $scope.createMoment = function() {
      
      ParseService.createMoment(
        $scope.newMomentTitle, 
        $scope.newMomentLocation,
        $scope.newMomentDate,
        {
          success: function(object) {
            $scope.$apply(function() {
              console.log(object.id);
              $('#createMoment').modal('hide');
              $location.path("/edit/"+ object.id);
            });

          },
          error: function(model, error) {
            alert('There was a problem creating this moment.  Please try again later.');
          }
        });
    }; // createMoment

    $scope.init();

  });
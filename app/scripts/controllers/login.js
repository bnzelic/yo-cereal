'use strict';

angular.module('yomomentsMobApp')
	.controller('LoginCtrl', function($scope, $location, $route, ParseService) {
  		$scope.fp = false;
        $scope.toggleForgotPassword = function() {
            $scope.fp = $scope.fp === false ? true: false;
        };

		$scope.init = function() {
			if (ParseService.getUser()) {
				$location.path('/home');
			}
		};
		
		//Normal Log In
		$scope.logInUser = function() {
			//$scope.password = 'bob';
			//$scope.username = 'bob';
			var username = angular.lowercase($scope.username);
			ParseService.login(
				username, $scope.password, {
					success: function(user) {
						$scope.$apply(function() {
							$location.path("/home");
						});
					},

					error: function(user, error) {
						
					}
				});
		};

		//Log In via Facebook - NOT BEING USED
		$scope.logInFB = function() {
			ParseService.FB_login({
				success: function(user) {
					$scope.$apply(function() {
						$location.path("/home");
					});
				},

				error: function(user, error) {
					alert('Facebook Login Failed');
				}
			});
		};

		//Forgot Password Function
		$scope.resetPassword = function() {
			ParseService.resetPassword(
				$scope.resetEmail, {
					success: function(user) {
						$scope.$apply(function() {
							alert('Password has been reset. Check your email.');
							$route.reload();
						});
					},
					error: function(user, error) {
						alert('Oops something went wrong here');
						$route.reload();
					}
				});
		};

		$scope.createUser = function() {
			ParseService.signUp(
				$scope.username, $scope.password, $scope.fullName, {
					success: function(user) {
						$scope.$apply(function() {
							$location.path("/home");
						});
					},
					error: function(user, error) {
						alert('Oops something went wrong here');
					}
				});
		};

		$scope.init();
	});
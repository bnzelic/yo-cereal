'use strict';

angular.module('yomomentsMobApp')
  .controller('MainCtrl', function($location, $scope, $routeParams, ParseService) {
    $scope.go = function (path, id) { 
      $location.path(path+id);
    };
    $scope.goBack = function () {
      history.back();
      $scope.$apply();
    };
    $scope.logOutUser = function () {
      ParseService.logout();
      $location.path('/');
    };
    $scope.categories = ["Moment", "Team", "Class", "Group", "Event", "Fraternity", "Sorority", "Company"];

  });

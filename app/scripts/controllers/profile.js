'use strict';

angular.module('yomomentsMobApp').controller('ProfileCtrl', function($scope, $location, ParseService) {

    $scope.userName = Parse.User.current().getUsername();
    $scope.email = Parse.User.current().getEmail();
    $scope.fullName = Parse.User.current().get('fullName');
    $scope.avatar = Parse.User.current().get('avatar');
    $scope.twitterId = Parse.User.current().get('twitterID');
    $scope.facebookId = Parse.User.current().get('facebookId');
    $scope.instagramId = Parse.User.current().get('instagramID');
    $scope.linkedinId = Parse.User.current().get('linkedinId');

    /*$scope.navigation = angular.uppercase($scope.userName);*/
    /*$scope.navigation = "Profile";*/

    ParseService.getUser({
      success: function(profile) {
        $scope.$apply(function() {
          return $scope.profile;
        });

      },
      error: function(profile) {
        alert('Error getting profile');
      }
    });

    $scope.createUser = function() {
      $scope.errorMsgs = [];

      if ($scope.password !== $scope.passwordConfirm) {
        $scope.errorMsgs.push("Passwords don't match");
      } else {
        $scope.msg = 'Saving account...';

        ParseService.signUp(
          $scope.email, $scope.password, $scope.fullName, {
            success: function(user) {
              $scope.$apply(function() {
                $location.path("/moments");
              });
            },

            error: function(user, error) {
              alert('There was a problem creating your account.  Please try again later.');
            }
          });

      }
    };


    //Update User Data
    $scope.modifyUser = function() {
      ParseService.updateUser($scope.fullName, $scope.username, $scope.email, $scope.password, $scope.facebookId, $scope.twitterId, $scope.linkedinId, $scope.instagramId, {
            success: function(user) {
              $scope.$apply(function() {
                $location.path("/profile");
              });
            },

            error: function(user, error) {
              alert('There was a problem saving profile');
            }
          });

    };

  


$scope.init();


});
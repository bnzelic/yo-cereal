'use strict';

angular.module('yomomentsMobApp')
  .controller('CompositeCreateCtrl', function($scope, $location, ParseService, $routeParams, $timeout, ParseCereal, mvLayout) {
    $scope.getMoments = function() {
      ParseService.getMoments({
        success: function(moments) {
          $scope.$apply(function() {
            $scope.moments = moments.models;
          });
        }, //success
        error: function(moments) {
          alert('Error getting moments');
        }

      }); //getMoments
    }

    $scope.layouts = undefined;

    $scope.loadAllLayouts = function() {
      ParseCereal.loadAllLayouts()
        .then(function(layouts) {
          $scope.layouts = layouts;
          //reset spinner
        })
    };

    $scope.loadLayout = function(rows) {
         mvLayout.loadLayout($scope.data.composite, $scope.headshots, rows)
             .then(function() {
                $scope.init()
         });
    };

    $scope.loadLayout1 = function() {
        mvLayout.setLayout1($scope.data.composite, $scope.headshots)
            .then(function() {
                $scope.init()
            });
    };
	
	$scope.loadTriangleUpLayout = function() {
        mvLayout.alignHeadshotsAsTriangleUp($scope.data.composite, $scope.headshots)
            .then(function() {
                $scope.init();
            });
    };
	
	$scope.loadTriangleDownLayout = function() {
        mvLayout.alignHeadshotsAsTriangleDown($scope.data.composite, $scope.headshots)
            .then(function() {
                $scope.init();
            });
    };

  $scope.loadDiamondLayout = function() {
      mvLayout.alignHeadshotsAsDiamond($scope.data.composite, $scope.headshots)
        .then(function() {
          $scope.init();
        });
    };
	
	$scope.loadCircleLayout = function() {
        mvLayout.alignHeadshotsInCircle($scope.data.composite, $scope.headshots)
            .then(function() {
                $scope.init();
            });
    };	
	

    $scope.init = function() {
      //Check If user Logged In & Log Out If Not
      /*if (!ParseService.getUser()) {
        $location.path('/');
      }*/

      $scope.data = {};
      $scope.data.composite = {};
      $scope.getMoments();
      $scope.headshots = {};


      ParseService.getMoment($routeParams.momentId, {
        success: function(moment) {

          ParseService.getHeadshotsInMoment({
            success: function(headshots) {
              $scope.$apply(function() {
                $scope.headshots = headshots;
                $scope.data.composite.headshots = headshots.toArray();
                $scope.data.composite.moment = moment;
                console.log(moment);
              });
            },
            error: function(headshot, error) {
              alert('There was a problem getting these headshots.  Please try again later.');
            }

          }); //end getHeadshotsInMoment
        },
        error: function(moment, error) {
          if (!ParseService.getUser()) {
            $location.path('/');
          }
          else {
            alert('There was a problem getting this moment.  Please try again later.');
          }
          
        }

      }); //end getMoment
    }; //end init

    $scope.reset = function() {
      ParseService.resetHeadshotXY($routeParams.momentId, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem uPDATING this MOMENT.  Please try again later.');
          }
        });
    }; //end reset

    //Change Headshot Shape - string
    $scope.ccHeadshotShape = function(shape) {
      var moment = $scope.data.composite.moment;
      var attr = "headshotBorderType";
      ParseService.updateMoment1(moment.id, attr, shape, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Toggle Name On - boolean
    $scope.ccHeadshotName = function() {
      var moment = $scope.data.composite.moment;
      var attr = "headshotNameOn";
      var value = moment.attributes.headshotNameOn;
      value = !value;
      
      ParseService.updateMoment1(moment.id, attr, value, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert(error);
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Toggle Title On - boolean
    $scope.ccHeadshotTitle = function() {
      var moment = $scope.data.composite.moment;
      var attr = "headshotTitleOn";
      var value = moment.attributes.headshotTitleOn;
      value = !value;
      
      ParseService.updateMoment1(moment.id, attr, value, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert(error);
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Border Width - int
    $scope.ccHeadshotBorderWidth = function() {
      var num = document.getElementById('newHeadshotBorderWidth').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "headshotBorderWidth";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Border Color - string
    $scope.ccHeadshotBorderColor = function() {
      var color = document.getElementById('newHeadshotBorderColor').value;
      var moment = $scope.data.composite.moment;
      var attr = "headshotBorderColor";
      ParseService.updateMoment1(moment.id, attr, color, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Headshot Size - int
    $scope.ccHeadshotSize = function() {
      var num = document.getElementById('newHeadshotSize').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "headshotSize";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotSize

    //Change Headshot Shadow - string
    $scope.ccHeadshotShadow = function() {
      var moment = $scope.data.composite.moment;
      var attr = "headshotShadow";
      var shadow = document.getElementById('newHeadshotShadow').value;
      ParseService.updateMoment1(moment.id, attr, shadow, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape


    //Change Headshot Name FOnt Size - int
    $scope.ccHeadshotNameFontSize = function() {
      var num = document.getElementById('newHeadshotNameFontSize').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "headshotNameFontSize";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Headshot Name Font Color - string
    $scope.ccHeadshotNameFontColor = function() {
      var color = document.getElementById('newHeadshotNameFontColor').value;
      var moment = $scope.data.composite.moment;
      var attr = "headshotNameFontColor";
      ParseService.updateMoment1(moment.id, attr, color, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Headshot Title FOnt Size - int
    $scope.ccHeadshotTitleFontSize = function() {
      var num = document.getElementById('newHeadshotTitleFontSize').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "headshotTitleFontSize";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Headshot Title Font Color - string
    $scope.ccHeadshotTitleFontColor = function() {
      var color = document.getElementById('newHeadshotTitleFontColor').value;
      var moment = $scope.data.composite.moment;
      var attr = "headshotTitleFontColor";
      ParseService.updateMoment1(moment.id, attr, color, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Toggle Logo On - boolean
    $scope.ccLogoOn = function() {
      var moment = $scope.data.composite.moment;
      var attr = "headshotNameOn";
      var value = moment.attributes.headshotNameOn;
      value = !value;
      
      ParseService.updateMoment1(moment.id, attr, value, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert(error);
          }
        }); //end Parse Service
    }//end ccHeadshotShape


    //Change Logo Size - int
    $scope.ccLogoSize = function() {
      var num = document.getElementById('newLogoSize').value;
      num = parseFloat(num);
      var moment = $scope.data.composite.moment;
      var attr = "logoScale";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Composite Border Width - int
    $scope.ccMomentBorderWidth = function() {
      var num = document.getElementById('newMomentBorderWidth').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "momentBorderWidth";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Composite Border Color - string
    $scope.ccMomentBorderColor = function() {
      var color = document.getElementById('newMomentBorderColor').value;
      var moment = $scope.data.composite.moment;
      var attr = "momentBorderColor";
      ParseService.updateMoment1(moment.id, attr, color, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Composite Margin Width - int
    $scope.ccMomentBorderMargin = function() {
      var num = document.getElementById('newMomentBorderMargin').value;
      num = Math.floor(num);
      var moment = $scope.data.composite.moment;
      var attr = "momentBorderMargin";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    //Change Composite Background Color - string
    $scope.ccBackgroundColor = function() {
      var color = document.getElementById('newBackgroundColor').value;
      var moment = $scope.data.composite.moment;
      var attr = "backgroundColor";
      ParseService.updateMoment1(moment.id, attr, color, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape

    $scope.ccCompositeSize = function(size) {
       if (size == 'eight') {
          console.log("hey");
        } else if (size == 'eleven') {
          
        } else if (size == 'square') {

        }
      
      var moment = $scope.data.composite.moment;
      var attr = "momentBorderMargin";
      ParseService.updateMoment1(moment.id, attr, num, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end ccHeadshotShape


    //Update Moment OLD CODE TO BE DELETED
    $scope.updateMoment = function() {
      var moment = $scope.data.composite.moment;
      console.log("updating moment"+ moment.title);

      ParseService.updateMoment(moment.id,
        moment.attributes.backgroundColor,
        moment.attributes.background,
        moment.attributes.backgroundImageOn,
        moment.attributes.backgroundTile,
        moment.attributes.logoScale,
        moment.attributes.location,
        moment.attributes.date,
        moment.attributes.headshotSize,
        moment.attributes.locationOn,
        moment.attributes.locationFontColor,
        moment.attributes.locationFontSize,
        moment.attributes.locationFontType,
        moment.attributes.dateOn,
        moment.attributes.dateFontColor,
        moment.attributes.dateFontSize,
        moment.attributes.dateFontType,
        moment.attributes.headshotNameOn,
        moment.attributes.headshotNameFontColor,
        moment.attributes.headshotNameFontSize,
        moment.attributes.headshotNameFontType,
        moment.attributes.headshotTitleOn,
        moment.attributes.headshotTitleFontColor,
        moment.attributes.headshotTitleFontSize,
        moment.attributes.headshotTitleFontType,
        moment.attributes.logo,
        moment.attributes.logoBorder,
        moment.attributes.logoBorderColor,
        moment.attributes.logoBorderWidth,
        moment.attributes.headshotBorderType,
        moment.attributes.headshotBorderWidth,
        moment.attributes.headshotBorderColor,
        moment.attributes.momentBorderWidth,
        moment.attributes.momentBorderColor,
        moment.attributes.momentBorderMargin,
        moment.attributes.grid_w,
        moment.attributes.grid_h, {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }//end updateMoment

    $scope.saveComposite = function() {
      var moment = $scope.data.composite.moment;
        moment.attributes.momentBorderWidth = 20;
        moment.attributes.momentBorderColor = 'black';


      ParseService.updateMoment(moment.id,
        moment.attributes.momentBorderWidth,
        moment.attributes.momentBorderColor,
        {
          success: function(object) {
            $scope.init();
          },
          error: function(model, error) {
            alert('There was a problem updating Moment');
          }
        }); //end Parse Service
    }

    $scope.ccSave = function() {
      console.log("test");
      /*var img = stage.toDataURL("image/png");
      document.write('<img src="'+img+'"/>');
      */
      var canvas = document.getElementById('container');
      var context = canvas.getContext('2d');
      var strDataURI = canvas.toDataURL();
      Canvas2Image.saveAsPNG(canvas);
    }

    $scope.createTheme = function() {
      var theme = {};

      //strip out attributes that don't have theme attributes
      //needs to be extended if new attributes are created
	  for (var key in $scope.data.composite.moment.attributes) {
         //skip saving of stuff we don't need
         if (key === 'admin') continue;
         if (key === 'error') continue;
         if (key === 'logo') continue;
         if (key === 'success') continue;
         if (key === 'title') continue;
         if (key === 'vanity') continue;
 
         theme[key] = $scope.data.composite.moment.attributes[key];
       }

      ParseCereal.createTheme($scope.templateName, theme, {
          success: function(object) {
            console.log("template " + $scope.templateName + " - saved");
            //dismiss modal
            $('#saveTemplate').modal('hide');
          },
          error: function(model, error) {
            alert('There was a problem saving the Theme');
          }
        });
    }

	$scope.resetTheme = function() {

      for (var key in $scope.data.composite.moment.attributes) {
        if (key === 'admin') continue;
        if (key === 'error') continue;
        if (key === 'logo') continue;
        if (key === 'success') continue;
        if (key === 'title') continue;
        if (key === 'vanity') continue;
        
        $scope.data.composite.moment.attributes[key] = undefined;
      }
    }
	
    $scope.loadAllThemes = function() {
      console.log("loading themes");
      ParseCereal.loadAllThemes({
        success: function(obj) {
          console.log("themes loaded");
          // use $scope.$apply inside of non-Angular functions
          // in order to notify the DOM that the data has changed 
          $scope.$apply(function() { 
            $scope.themes = obj;
          });

        },
          error: function(model, error) {
            alert('There was a problem loading the Themes');
          }
      })
    }

    $scope.showThemeDetails = function(theme) {
      $scope.themeDetails = theme.attributes.data;
    }

    $scope.applyTheme = function(theme) {
      //show loading spinner
      $('.load .fa-spinner').show();
      $('.themeList').hide();

	  $scope.resetTheme();
      for (var key in theme.attributes.data) {

        //load all theme variabes
        $scope.data.composite.moment.attributes[key] = theme.attributes.data[key];
      }

      // apply theme to composite
      ParseCereal.applyTheme($routeParams.momentId, $scope.data.composite.moment.attributes, {
        success: function(obj) {
          $scope.init();
          $('#loadTemplate').modal('hide');
          $('.load .fa-spinner').hide();
          $('.themeList').show();
        },
        error: function(obj) {
		console.log(obj);
          alert('There was a problem applying the theme.')
        }
      })
    };

    $scope.init();
  });

'use strict';

angular.module('yomomentsMobApp')
  .controller('ThemeCtrl', function($scope, $location, ParseCereal, ParseService, $routeParams, $timeout) {

	//getThemes
	$scope.getThemes = function() {

      ParseCereal.loadAllThemes({
        success: function(obj) {
          // use $scope.$apply inside of non-Angular functions
          // in order to notify the DOM that the data has changed 
          $scope.themes = obj;
          $scope.$apply();
		  console.log($scope.themes);

        },
          error: function(model, error) { 
            alert('There was a problem loading the Themes');
          }
      })
	}

	$scope.ccHeadshotShape = function(shape) {
 	  $scope.data.composite.moment.attributes.headshotBorderType = shape;
	  $scope.data.composite.moment.changed = true;
	}
	
	//Toggle Name On - boolean
    $scope.ccHeadshotName = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Toggle Title On - boolean
    $scope.ccHeadshotTitle = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Border Width - int
    $scope.ccHeadshotBorderWidth = function() {
	  $scope.data.composite.moment.attributes.headshotBorderWidth = parseInt($scope.data.composite.moment.attributes.headshotBorderWidth);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Border Color - string
    $scope.ccHeadshotBorderColor = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Headshot Size - int
    $scope.ccHeadshotSize = function() {
	  $scope.data.composite.moment.attributes.headshotSize = parseInt($scope.data.composite.moment.attributes.headshotSize);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotSize

    //Change Headshot Shadow - string
    $scope.ccHeadshotShadow = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Headshot Name FOnt Size - int
    $scope.ccHeadshotNameFontSize = function() {
	  $scope.data.composite.moment.attributes.headshotNameFontSize = parseInt($scope.data.composite.moment.attributes.headshotNameFontSize);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Headshot Name Font Color - string
    $scope.ccHeadshotNameFontColor = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Headshot Title FOnt Size - int
    $scope.ccHeadshotTitleFontSize = function() {
	  $scope.data.composite.moment.attributes.headshotTitleFontSize = parseInt($scope.data.composite.moment.attributes.headshotTitleFontSize);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Headshot Title Font Color - string
    $scope.ccHeadshotTitleFontColor = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Toggle Logo On - boolean
    $scope.ccLogoOn = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Logo Size - int
    $scope.ccLogoSize = function() {
	  $scope.data.composite.moment.attributes.logoScale = parseInt($scope.data.composite.moment.attributes.logoScale);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Composite Border Width - int
    $scope.ccMomentBorderWidth = function() {
	  $scope.data.composite.moment.attributes.momentBorderWidth = parseInt($scope.data.composite.moment.attributes.momentBorderWidth);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Composite Border Color - string
    $scope.ccMomentBorderColor = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Composite Margin Width - int
    $scope.ccMomentBorderMargin = function() {
	  $scope.data.composite.moment.attributes.momentBorderMargin = parseInt($scope.data.composite.moment.attributes.momentBorderMargin);
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    //Change Composite Background Color - string
    $scope.ccBackgroundColor = function() {
	  $scope.data.composite.moment.changed = true;
    }//end ccHeadshotShape

    $scope.init = function() {
      if ($routeParams.themeId) {
        $scope.data = {};
        $scope.data.composite = {};

      //ID should ideally be replaced with a temporary moment
      // with placeholder images... and not archer
	  
	  if ($scope.data.composite.moment == null) { 
		  ParseService.getMoment("mLbNibmYTM", {
			success: function(moment) {

			  //console.log(ParseService.getCurrentMoment())

			  ParseService.getHeadshotsInMoment({
				success: function(headshots) {
				  $scope.data.composite.headshots = headshots.toArray();
				  $scope.data.composite.moment = moment;
				  //console.log(headshots);


				  //apply theme at this point
				  ParseCereal.getTheme($routeParams.themeId, {
					success: function(theme) {
					  //console.log("theme applied");    
						$scope.theme = theme;
						console.log($scope.theme);
						$scope.resetTheme();
						//console.log(theme);
						for (var attr in theme){
						  $scope.data.composite.moment.attributes[attr] = theme[attr];
						}
						$scope.$apply();
					}, 
					error: function() {
					  alert('Theme not able to be retrieved');
					}
				  })           
				},
				error: function(headshot, error) {
				  alert('There was a problem getting these headshots.  Please try again later.');
				}

			  }); //end getHeadshotsInMoment
			},
			error: function(moment, error) {
				alert('There was a problem getting this moment.  Please try again later.');          
			}
		
		  }); //end getMoment
		  
		}
    } else {
      $scope.getThemes();
    }
      
    

    }; //end init

	// Create a new Theme
    $scope.createTheme = function() {
      ParseCereal.createTheme(
        $scope.newThemeTitle,{}, //nodata
        {
          success: function(object) {
            $scope.$apply(function() {
              //console.log(object.id);
              $('#createTheme').modal('hide');
              $location.path("/themes/"+ object.id);
            });

          },
          error: function(model, error) {
            alert('There was a problem creating this theme.  Please try again later.');
          }
        });
    };

    $scope.saveTheme = function() {
      var theme = {};

      //strip out attributes that don't have theme attributes
      //needs to be extended if new attributes are created
      for (var key in $scope.data.composite.moment.attributes) {
        //skip saving of stuff we don't need
         if (key === 'admin') continue;
         if (key === 'error') continue;
         if (key === 'logo') continue;
         if (key === 'success') continue;
         if (key === 'title') continue;
         if (key === 'vanity') continue;
 
         theme[key] = $scope.data.composite.moment.attributes[key];

      }
	  theme.themeTitle = $scope.theme.themeTitle;

      ParseCereal.editTheme($routeParams.themeId, theme, {
          success: function(object) {
            //console.log("template " + $scope.templateName + " - saved");
			alert('Theme Saved');
            $scope.init();
            //show success message
			
          },
          error: function(model, error) {
            alert('There was a problem saving the Theme');
          }
        });
    }

    $scope.setSelected = function(theme) {
      $scope.selectedTheme = theme;
    }

    $scope.deleteTheme = function(theme) {
      ParseCereal.deleteTheme(theme.id, {
        success: function(object) {
          $('#deleteTheme').modal('hide');
          $scope.getThemes();
        },
        error: function(model, error) {
          alert('There was a problem deleting the Theme');
        }
      })

    }

    $scope.init();
	
    $scope.resetTheme = function() {

      for (var key in $scope.data.composite.moment.attributes) {
        if (key === 'admin') continue;
        if (key === 'error') continue;
        if (key === 'logo') continue;
        if (key === 'success') continue;
        if (key === 'title') continue;
        if (key === 'vanity') continue;
        
        $scope.data.composite.moment.attributes[key] = undefined;
      }
    }
  });
  

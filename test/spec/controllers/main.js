'use strict';

describe('Controller: ThemeCtrl', function() { 
	beforeEach(module('yomomentsMobApp'));
	
	var ThemeCtrl, scope;

	
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope.$new();
		ThemeCtrl = $controller('ThemeCtrl', {
			$scope: scope
		});
	}));
	
	it('Should display a list of themes', function() {
		expect(scope.themes.length).toBeGreaterThan(0);
	});
});


